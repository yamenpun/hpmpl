<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('entry_date');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('client_id');
            $table->string('patient_name');
            $table->string('gender');
            $table->string('age');
            $table->string('referred_by');
            $table->string('test_names');
            $table->double('amount', 10, 2);
            $table->string('serum_code');
            $table->string('blood_code');
            $table->string('urine_code');
            $table->string('fluoride_code');
            $table->string('others_title');
            $table->string('others_code');
            $table->string('remarks', 255)->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->boolean('is_camp')->default(0);
            $table->timestamps();

            // Foreign Key
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
