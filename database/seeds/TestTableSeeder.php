<?php

use Illuminate\Database\Seeder;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test')->insert([
            'user_id'       => '1',
            'name'          => 'TFTG',
            'description'   => 'This is TFTG Test',
            'rate'          => '3000',
        ]);

        DB::table('test')->insert([
            'user_id'       => '1',
            'name'          => 'FFT',
            'description'   => 'This is FFT Test',
            'rate'          => '4000',
        ]);

        DB::table('test')->insert([
            'user_id'       => '1',
            'name'          => 'VIR',
            'description'   => 'This is VIR test',
            'rate'          => '1500',
        ]);

        DB::table('test')->insert([
            'user_id'       => '1',
            'name'          => 'TTH',
            'description'   => 'This is TTH test',
            'rate'          => '1530',
        ]);

        DB::table('test')->insert([
            'user_id'       => '1',
            'name'          => 'VITT',
            'description'   => 'This is VITT test',
            'rate'          => '5575',
        ]);
    }
}
