<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 5;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('client')->insert([
                'user_id'   => '1',
                'name'      => $faker->company,
                'address'   => $faker->address,
                'phone'     => $faker->phoneNumber,
                'email'     => $faker->unique()->email,
                'created_at'=> $faker->dateTime,
                'updated_at'=> $faker->dateTime
            ]);
        }
    }
}


