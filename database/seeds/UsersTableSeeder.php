<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullname'      => 'Yamendra Pun',
            'username'      => 'yamenpun',
            'email'         => 'admin@gmail.com',
            'password'      => bcrypt('admin'),
            'role'          => 'administrator'
        ]);

        DB::table('users')->insert([
            'fullname'      => 'John Doe',
            'username'      => 'johndoe',
            'email'         => 'normal@gmail.com',
            'password'      => bcrypt('normal'),
            'role'          => 'normal'
        ]);


    }
}
