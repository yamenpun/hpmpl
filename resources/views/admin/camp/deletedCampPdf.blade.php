<!DOCTYPE html>
<html>
<head>
    <title>Convert To PDF</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: left;
            padding: 8px;
            font-size: 12px;
        }
    </style>
</head>
<body>

<div style="overflow-x:auto;">
    <h2 style="text-align: center">Deleted Camp Detail Information</h2>
    <table>
        <tr>
            <th style="width: 10px;">S.N.</th>
            <th style="width: 50px;">Entry Date</th>
            <th style="width: 60px;">Client Name</th>
            <th style="width: 60px;">Patient Name</th>
            <th style="width: 20px;">Gender</th>
            <th style="width: 20px;">Age</th>
            <th style="width: 60px;">Referred By</th>
            <th style="width: 50px;">Test Names</th>
            <th style="width: 50px;">Amount</th>
        </tr>
        <?php $i = 0 ?>
        @foreach($data['rows'] as $row)
            <?php $i++ ?>
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $row->entry_date }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->patient_name }}</td>
                <td>{{ $row->gender }}</td>
                <td>{{ $row->age }} years</td>
                <td>{{ $row->referred_by }}</td>
                <td>{{ $row->test_names }}</td>
                <td>Rs. {{ $row->amount }}</td>
            </tr>
        @endforeach
    </table>
</div>

</body>
</html>
