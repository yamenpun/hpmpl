@extends('admin.layouts.master')

@section('title')
    View Health Camp
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Health Camp List</a>
                </li>

                <li class="active">View Health Camp</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Health Camp Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">
                                    <thead>

                                        <tr role="row">

                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 60px;">Column Name
                                            </th>

                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 180px;">Column Value
                                            </th>

                                        </tr>

                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                        <tr class="odd">

                                            <td>Client Name</td>
                                            <td>{{ $data['row']['0']->name }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Patient Name</td>
                                            <td>{{ $data['row']['0']->patient_name }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Gender</td>
                                            <td>{{ $data['row']['0']->gender }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Age</td>
                                            <td>{{ $data['row']['0']->age }} years old</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Ref By</td>
                                            <td>{{ $data['row']['0']->referred_by }} </td>

                                        </tr>

                                        <tr class="even">
                                            <td>Test Name</td>
                                            <td>{{ $data['row']['0']->test_names }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Amount</td>

                                            <td>Rs. {{ $data['row']['0']->amount }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Serum Code</td>

                                            <td>
                                                @if($data['row']['0']->serum_code)
                                                    {{ $data['row']['0']->serum_code }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Blood Code</td>

                                            <td>
                                                @if($data['row']['0']->blood_code)
                                                    {{ $data['row']['0']->blood_code }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Fluoride Code</td>

                                            <td>
                                                @if($data['row']['0']->fluoride_code)
                                                    {{ $data['row']['0']->fluoride_code }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>
                                                @if($data['row']['0']->others_title)
                                                    {{ $data['row']['0']->others_title }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>

                                            <td>
                                                @if($data['row']['0']->others_code)
                                                    {{ $data['row']['0']->others_code }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>

                                        </tr>

                                        <tr class="even">
                                            <td>Remarks</td>
                                            <td>
                                                @if($data['row']['0']->remarks)
                                                    {{ $data['row']['0']->remarks }}
                                                @else
                                                    N/A
                                                @endif
                                            </td>
                                        </tr>

                                        <tr class="odd">

                                            <td>Created At</td>

                                            <td>{{ date('jS M, Y', strtotime($data['row']['0']->created_at)) }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Updated At</td>

                                            <td>{{ date('jS M, Y', strtotime($data['row']['0']->updated_at)) }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Created By</td>
                                            <td>{{ $data['row']['0']->user_name }}</td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
