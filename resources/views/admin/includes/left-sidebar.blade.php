<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('admin/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('admin/transactions/list*') || Request::is('admin/transactions/view*') || Request::is('admin/transactions/deletedView*') || Request::is('admin/transactions/add*') || Request::is('admin/transactions/newPatient*') || Request::is('admin/transactions/edit*') || Request::is('admin/transactions/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-credit-card"></i>
                <span class="menu-text"> Work Order </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/transactions/list*') || Request::is('admin/transactions/view*') || Request::is('admin/transactions/newPatient*') || Request::is('admin/transactions/add*') || Request::is('admin/transactions/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.transactions.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Work Order List
                    </a>

                </li>

                <li {!! Request::is('admin/transactions/deletedList*') || Request::is('admin/transactions/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.transactions.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Work Order List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/camp/list*') || Request::is('admin/camp/view*') || Request::is('admin/camp/deletedView*') || Request::is('admin/camp/add*') || Request::is('admin/camp/newPatient*') || Request::is('admin/camp/edit*') || Request::is('admin/camp/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-plus-sign-alt"></i>
                <span class="menu-text"> Health Camp </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/camp/list*') || Request::is('admin/camp/view*') || Request::is('admin/camp/newPatient*') || Request::is('admin/camp/add*') || Request::is('admin/camp/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.camp.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Health Camp List
                    </a>

                </li>

                <li {!! Request::is('admin/camp/deletedList*') || Request::is('admin/camp/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.camp.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Health Camp List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/payments/list*') || Request::is('admin/payments/view*') || Request::is('admin/payments/deletedView*') || Request::is('admin/payments/add*') || Request::is('admin/payments/edit*') || Request::is('admin/payments/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-shopping-cart"></i>
                <span class="menu-text"> Payments </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/payments/list*') || Request::is('admin/payments/view*') || Request::is('admin/payments/add*') || Request::is('admin/payments/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.payments.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Payments List
                    </a>

                </li>

                <li {!! Request::is('admin/payments/deletedList*') || Request::is('admin/payments/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.payments.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Payments List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/client/list*') || Request::is('admin/client/view*') || Request::is('admin/client/add*') || Request::is('admin/client/edit*') || Request::is('admin/client/deletedList*') || Request::is('admin/client/deletedView*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-group"></i>
                <span class="menu-text"> Client </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/client/list*') || Request::is('admin/client/view*') || Request::is('admin/client/add*') || Request::is('admin/client/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.client.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Client List
                    </a>

                </li>

                <li {!! Request::is('admin/client/deletedList*') || Request::is('admin/client/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.client.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled Client List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/test/list*') || Request::is('admin/test/view*')|| Request::is('admin/test/deletedView*') || Request::is('admin/test/add*') || Request::is('admin/test/edit*') || Request::is('admin/test/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-beaker"></i>
                <span class="menu-text"> Test </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/test/list*') || Request::is('admin/test/view*') || Request::is('admin/test/add*') || Request::is('admin/test/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.test.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Test List
                    </a>

                </li>

                <li {!! Request::is('admin/test/deletedList*') || Request::is('admin/test/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.test.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled Test List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/log/list*') || Request::is('admin/log/deleteForm*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-info-sign "></i>
                <span class="menu-text"> Activity Log </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/log/list*') ?'class="active"':'' !!}>

                    <a href="{{ route('admin.log.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Activity Log List
                    </a>

                </li>

                <li {!! Request::is('admin/log/deleteForm*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.log.deleteForm') }}">
                        <i class="icon-double-angle-right"></i>
                        Delete Activity Log
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/user*')?'class="active"':'' !!}>

            <a href="{{ route('admin.user') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

