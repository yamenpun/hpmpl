@extends('admin.layouts.master')

@section('title')
    Add Test
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Test List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Test Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-sm-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route' => $base_route.'.store',
                    'method' => 'post',
                    'class' => 'form-horizontal',
                    'role' => "form",
                    'enctype' => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="name">Test Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-8">
                            {!! Form::text('name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'name',
                               "placeholder"                        => "Test Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'name') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="rate">Test Rate <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-8">

                            {!! Form::text('rate', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'rate',
                               "placeholder"                        => "Rate",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'rate') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="description">Description</label>

                        <div class="col-sm-8">

                            {!! Form::textarea('description', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'description',
                               "class"                              => "col-xs-10 col-sm-12 form-control mceEditor",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'description') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection

@section('page_specific_scripts')
    @include('admin.common.editor')
@endsection