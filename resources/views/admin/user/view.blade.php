@extends('admin.layouts.master')

@section('title')
    View User
@endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">User List</a>
                </li>

                <li class="active">View User</li>
            </ul>


        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        User Detail Info
                    </small>

                    <div class="btn-group">

                        <a href="{{ route($base_route.'.add') }}" class="btn btn-primary btn-sm">
                            <i class="icon-plus-sign bigger-110"></i>
                            Add New
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm ">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-8">
                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">
                                    <thead>

                                        <tr role="row">
                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 60px;">Column Name
                                            </th>
                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 180px;">Column Value
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        <tr class="even">
                                            <td>Full Name</td>
                                            <td>{{ $data['row']->fullname }} </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>Username</td>
                                            <td>{{ $data['row']->username }} </td>
                                        </tr>
                                        <tr class="even">
                                            <td>Email</td>
                                            <td>{{ $data['row']->email }} </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>Role</td>
                                            <td>{{ $data['row']->role }} </td>
                                        </tr>
                                        <tr class="odd">
                                            <td>Created At</td>
                                            <td>{{ date('jS M, Y', strtotime($data['row']->created_at)) }} </td>
                                        </tr>
                                        <tr class="even">
                                            <td>Updated At</td>
                                            <td>{{ date('jS M, Y', strtotime($data['row']->updated_at)) }} </td>
                                        </tr>
                                        <tr class="even">
                                            <td>Status</td>
                                            <td>
                                                @if($data['row']->status == 1)
                                                    <button class="btn btn-minier btn-primary">Active</button>
                                                @else
                                                    <button class="btn btn-minier btn-yellow">Inactive</button>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>

                            </div>

                        </div>
                    </div>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection
