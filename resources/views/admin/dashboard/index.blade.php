@extends('admin.layouts.master')

@section('page_specific_style')

    <style>
        .acb {
            display: block;
        }
    </style>

@endsection

@section('title')
    Dashboard
@endsection

@section('content')


    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li class="active">Dashboard</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Welcome To Dashboard Manager
                    </small>

                </h1>

            </div>


            <div class="col-xs-12">

                <div class="alert alert-block alert-success">

                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>

                    <i class="icon-ok green"></i>

                    Hi, <b>{{ ucfirst($logged_in_user->fullname) }}</b> welcome to the dashboard of Health Park Medico Pvt. Ltd.

                </div>

                <div class="col-xs-12">

                    <div class="col-xs-9 infobox-container">

                        <div class="infobox infobox-blue">
                            <div class="infobox-icon">
                                <i class="icon-plus"></i>
                            </div>

                            <div class="infobox-data">
                                <div class="infobox-content"><a href="{{ route('admin.transactions.add') }}"><h5>ENTRY FORM</h5></a></div>
                            </div>
                        </div>

                        <div class="infobox infobox-blue">
                            <div class="infobox-icon">
                                <i class="icon-plus"></i>
                            </div>

                            <div class="infobox-data">
                                <div class="infobox-content"><a href="{{ route('admin.camp.add') }}"><h5>ADD HEALTH CAMP</h5></a></div>
                            </div>
                        </div>

                        <div class="infobox infobox-blue">
                            <div class="infobox-icon">
                                <i class="icon-plus"></i>
                            </div>

                            <div class="infobox-data">
                                <div class="infobox-content"><a href="{{ route('admin.payments.add') }}"><h5>ADD PAYMENT</h5></a></div>
                            </div>
                        </div>

                        <div class="space-6"></div>

                        <div class="infobox infobox-green">
                            <div class="infobox-icon">
                                <i class="icon-group"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{ $data['no_of_clients']['0']->no_of_clients }}</span>
                                <div class="infobox-content"><a href="{{ route('admin.client.list') }}"><h5>CLIENTS</h5></a></div>
                            </div>
                        </div>

                        <div class="infobox infobox-red">
                            <div class="infobox-icon">
                                <i class="icon-beaker"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{ $data['no_of_tests']['0']->no_of_tests }}</span>
                                <div class="infobox-content"><a href="{{ route('admin.test.list') }}"><h5>TESTS</h5></a></div>
                            </div>
                        </div>

                        <div class="infobox infobox-blue">
                            <div class="infobox-icon">
                                <i class="icon-credit-card"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{ $data['no_of_entry']['0']->no_of_entry }}</span>
                                <div class="infobox-content"><a href="{{ route('admin.test.list') }}"><h5>WORK ORDERS</h5></a></div>
                            </div>
                        </div>

                        <div class="space-6"></div>

                        <div class="infobox infobox-green infobox-large infobox-dark">
                            <div class="infobox-data">
                                <div class="infobox-content">Total Transaction Amount</div>
                                <div class="infobox-content">
                                    @foreach($data['total_amount_to_be_paid'] as $total_amount_to_be_paid)
                                        @if($total_amount_to_be_paid->total_amount_to_be_paid)
                                            Rs. {{ $total_amount_to_be_paid->total_amount_to_be_paid }}
                                        @else
                                            Rs. 0
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="infobox infobox-orange infobox-large infobox-dark">
                            <div class="infobox-data">
                                <div class="infobox-content">Total Paid Amount</div>
                                <div class="infobox-content">
                                    @foreach($data['total_amount_paid'] as $total_amount_paid)
                                        @if($total_amount_paid->total_amount_paid)
                                            Rs. {{ $total_amount_paid->total_amount_paid }}
                                        @else
                                            Rs. 0
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="infobox infobox-red infobox-large infobox-dark">
                            <div class="infobox-data">
                                <div class="infobox-content">Total Due Amount</div>
                                <div class="infobox-content">
                                    @foreach($data['total_amount_to_be_paid'] as $total_amount_to_be_paid)
                                        @foreach($data['total_amount_paid'] as $total_amount_paid)
                                            Rs. {{ $total_amount_to_be_paid->total_amount_to_be_paid - $total_amount_paid->total_amount_paid }}
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

