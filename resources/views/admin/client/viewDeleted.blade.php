@extends('admin.layouts.master')

@section('title')
    View Disabled Client
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.deletedList') }}">Disabled Client List</a>
                </li>

                <li class="active">View Disabled Client</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Disabled Client Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm ">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">

                                    <thead>

                                    <tr role="row">

                                        <th role="columnheader" tabindex="0"
                                            aria-controls="sample-table-2" rowspan="1" colspan="1"
                                            style="width: 60px;">Column Name
                                        </th>

                                        <th role="columnheader" tabindex="0"
                                            aria-controls="sample-table-2" rowspan="1" colspan="1"
                                            style="width: 180px;">Column Value
                                        </th>

                                    </tr>

                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                    <tr class="even">

                                        <td>Client Name</td>

                                        <td>{{ $data['row']->name }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Client Address</td>

                                        <td>{{ $data['row']->address }} </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Client Phone</td>

                                        <td>{{ $data['row']->phone }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Client Email</td>

                                        <td>{{ $data['row']->email }} </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Created At</td>

                                        <td>{{ date('jS M, Y', strtotime($data['row']->created_at)) }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Updated At</td>

                                        <td>{{ date('jS M, Y', strtotime($data['row']->updated_at)) }} </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Created By</td>

                                        @foreach($data['user'] as $user)
                                            @if($user->id == $data['row']->user_id)
                                                <td>{{ $user->fullname }}</td>
                                            @endif
                                        @endforeach

                                    </tr>

                                    </tbody>

                                </table>

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th colspan="6"><p align="center">List Of All Transactions</p></th>
                                    </tr>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Created Date</th>
                                        <th>Patient Name</th>
                                        <th>Test Names</th>
                                        <th>Amount</th>
                                        <th>Created By</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($data['transactions'] as $transaction)
                                        <?php $i++ ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $transaction->entry_date }}</td>
                                            <td>{{ $transaction->patient_name }}</td>
                                            <td>{{ $transaction->test_names }}</td>
                                            <td>Rs. {{ $transaction->amount }}</td>
                                            <td>{{ $transaction->fullname }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>

                                </table>

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th colspan="5"><p align="center">List Of All Payments</p></th>
                                    </tr>
                                    <tr>
                                        <th>S.N.</th>
                                        <th>Created Date</th>
                                        <th>Client Name</th>
                                        <th>Amount</th>
                                        <th>Created By</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $i = 0 ?>
                                    @foreach($data['payments'] as $payment)
                                        <?php $i++ ?>
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $payment->created_date }}</td>
                                            <td>{{ $payment->name }}</td>
                                            <td>Rs. {{ $payment->amount }}</td>
                                            <td>{{ $payment->fullname }}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
