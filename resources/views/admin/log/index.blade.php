@extends('admin.layouts.master')

@section('title')
    User Log List
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li class="active">User Log List</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        User Log List
                    </small>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                                    <thead>

                                        <tr>
                                            <th style="width: 10px;" class="center">

                                                <label>
                                                    <input type="checkbox" class="ace" />
                                                    <span class="lbl"></span>
                                                </label>

                                            </th>

                                            <th style="width: 100px;">User</th>

                                            <th style="width: 100px;">Action</th>

                                            <th style="width: 200px;">Description</th>

                                            <th style="width: 100px;">Action Date</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        @foreach($data['rows'] as $row)

                                            <tr>

                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>

                                                <td>{{ $row->fullname }}</td>

                                                <td>{{ $row->action }}</td>

                                                <td>{{ $row->description }}</td>

                                                <td>{{ $row->created_at }}</td>

                                            </tr>

                                        @endforeach

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- page specific plugin scripts -->

    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/jquery.dataTables.bootstrap.js') }}"></script>

    <script type="text/javascript">

        jQuery(function($) {

            var oTable1 = $('#sample-table-2').dataTable( {

                "aoColumns": [

                    { "bSortable": false },

                    null, null, null,

                    { "bSortable": false }

                ] } );


            $('table th input:checkbox').on('click' , function(){

                var that = this;

                $(this).closest('table').find('tr > td:first-child input:checkbox')

                    .each(function(){

                        this.checked = that.checked;

                        $(this).closest('tr').toggleClass('selected');

                    });

            });


            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});

            function tooltip_placement(context, source) {

                var $source = $(source);

                var $parent = $source.closest('table')

                var off1 = $parent.offset();

                var w1 = $parent.width();

                var off2 = $source.offset();

                var w2 = $source.width();

                if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';

                return 'left';
            }
        })

    </script>

@endsection
