@extends('admin.layouts.master')

@section('title')
    Update Work Order
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ asset('assets/admin/css/chosen.css') }}" />

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Work Order List</a>
                </li>

                <li class="active">Update Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Update Work Order Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-sm-12">

                    {!! Form::model($data['row'], [
                        'route'     => [$base_route.'.update', $data['row']->id],
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'role'      => "form",
                        'enctype'   => "multipart/form-data"
                        ]) !!}

                    <input type="hidden" name="id" value="{{ $data['row']->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="entry_date">Entry Date <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div class="input-group">

                                <input type="text" name="entry_date" data-date-format="dd-mm-yyyy" value="{{ $data['row']->entry_date }}" id="id-date-picker-1" class="form-control date-picker">

                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                            </div>

                            {!! AppHelper::getValidationErrorMsg($errors, 'entry_date') !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="client_id">Client Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12" name="client_id"
                                    id="client_id" class="form-control">

                                @foreach($data['client'] as $parent)
                                    @if($data['row']->client_id === $parent->id)
                                        <option value="{{ $parent->id }}" selected="selected">{{ $parent->name }}</option>
                                    @else
                                        <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                    @endif
                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="patient_name">Patient Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('patient_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'patient_name',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'patient_name') !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="age">Age <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::number('age', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'age',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="gender">Gender <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div class="control-group">

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('gender', 'Male', true, ['class' => 'ace']) !!}
                                        <span class="lbl">Male</span>
                                    </label>

                                    <label>
                                        {!! Form::radio('gender', 'Female', false, ['class' => 'ace']) !!}
                                        <span class="lbl">Female</span>
                                    </label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="referred_by">Ref By <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('referred_by', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'referred_by',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="test_names">Test Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select name="test_names[]" multiple="" class="width-80 chosen-select" data-placeholder="Choose Test...">

                                @foreach($data['tests'] as $test)

                                    <option value="{{ $test->name }}">{{ $test->name }}</option>

                                @endforeach
                                
                            </select>

                        </div>

                        <div class="col-sm-2"><p class="blue">{{ $data['row']->test_names }}</p></div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="amount">Amount <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('amount', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'amount',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'rate') !!}

                        </div>

                    </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="serum_code">Serum Code <span style="color: #bf1d1d;">*</span></label>

                            <div class="col-sm-6">

                                {!! Form::text('serum_code', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'serum_code',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="blood_code">Blood Code</label>

                            <div class="col-sm-6">

                                {!! Form::text('blood_code', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'blood_code',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                            </div>

                        </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="urine_code">Urine Code</label>

                        <div class="col-sm-6">

                            {!! Form::text('urine_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'urine_code',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="fluoride_code">Fluoride Code</label>

                            <div class="col-sm-6">

                                {!! Form::text('fluoride_code', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'fluoride_code',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="others_title">Others Title</label>

                            <div class="col-sm-6">

                                {!! Form::text('others_title', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'others_title',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label no-padding-right" for="others_code">Others Code</label>

                            <div class="col-sm-6">

                                {!! Form::text('others_code', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'others_code',
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                            </div>

                        </div>

                    <div class="space-4"></div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"
                               for="remarks"> Remarks </label>

                        <div class="col-sm-6">

                            {!! Form::text('remarks', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'remarks',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'remarks') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Update
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

   <script src="{{ asset('assets/admin/js/chosen.jquery.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $(".chosen-select").chosen();
        });
    </script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        });
    </script>


@endsection