@extends('admin.layouts.master')

@section('title')
    Payment List
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/daterangepicker.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li class="active">Payment List</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Payment List
                    </small>

                    <div class="btn-group">

                        <a href="{{ route($base_route.'.add') }}" class="btn btn-primary btn-sm">
                            <i class="icon-plus-sign bigger-110"></i>
                            Add New
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <a target="_blank"
                           href="{{ route($base_route.'.print', ['client' => AppHelper::getClientFilter(), 'date-range' => AppHelper::getDateRangeFilter() ] ) }}"
                           class="btn btn-pink btn-sm">
                            <i class="icon-print bigger-110"></i>
                            Print
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">
                            Export As
                            <i class="icon-angle-down icon-on-right"></i>
                        </button>

                        <ul class="dropdown-menu">

                            <li><a target="_blank"
                                   href="{{ route($base_route.'.pdf', ['client' => AppHelper::getClientFilter(), 'date-range' => AppHelper::getDateRangeFilter() ] ) }}">PDF</a>
                            </li>

                            <li class="divider"></li>

                            <li><a target="_blank"
                                   href="{{ route($base_route.'.excel', ['client' => AppHelper::getClientFilter(), 'date-range' => AppHelper::getDateRangeFilter() ] ) }}">Excel</a>
                            </li>

                        </ul>

                    </div>

                </h1>

            </div>

            <div class="page-header">

                <h6>

                    {!! Form::open(['method'  => 'GET']) !!}

                    <label> Client </label>

                    <select name="client">

                        <option value="all" selected="selected">-- Select Client --</option>

                        @foreach($data['clients'] as $client)

                            <option value="{{ $client->name }}" {!! Request()->get('client')== $client->name?'selected=selected':'' !!} >{{ $client->name }}</option>

                        @endforeach

                    </select>

                    <label> Date </label>

                    <input type="text" name="date-range" placeholder="Select Date"/>

                    <button class="btn btn-pink btn-sm" type="submit">

                        <i class="icon-filter bigger-110"></i>

                        Filter

                    </button>

                    {!! Form::close() !!}

                </h6>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th style="width: 10px;" class="center">
                                            <label>
                                                <input type="checkbox" class="ace"/>
                                                <span class="lbl"></span>
                                            </label>
                                        </th>

                                        <th style="width: 100px;">Created Date</th>

                                        <th style="width: 200px;">Client Name</th>

                                        <th style="width: 200px;">Paid Amount</th>

                                        <th style="width: 100px;">Created By</th>

                                        <th style="width: 100px;"></th>

                                    </tr>

                                    </thead>

                                    <tbody>
                                    @foreach($data['rows'] as $row)

                                        <tr>

                                            <td class="center">
                                                <label>
                                                    <input type="checkbox" class="ace"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </td>

                                            <td>{{ $row->created_date }}</td>

                                            <td>{{ $row->name }}</td>

                                            <td>Rs. {{ $row->amount }}</td>

                                            <td>{{ $row->fullname }}</td>

                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                                    <a class="blue"
                                                       href="{{ route($base_route.'.view', ['id' => $row->id]) }}">
                                                        <i class="icon-zoom-in bigger-130" title="View Payment"></i>
                                                    </a>

                                                    <a class="green"
                                                       href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                        <i class="icon-pencil bigger-130" title="Update Payment"></i>
                                                    </a>

                                                    <a class="red deleteConfirm"
                                                       href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                        <i class="icon-trash bigger-130" title="Delete Payment"></i>
                                                    </a>

                                                </div>

                                                <div class="visible-xs visible-sm hidden-md hidden-lg">

                                                    <div class="inline position-relative">

                                                        <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <i class="icon-caret-down icon-only bigger-120"></i>
                                                        </button>

                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                            <li>
                                                                <a href="#" class="tooltip-info" data-rel="tooltip"
                                                                   title="View">
                                                                    <span class="blue">
                                                                        <i class="icon-zoom-in bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="#" class="tooltip-success" data-rel="tooltip"
                                                                   title="Edit">
                                                                    <span class="green">
                                                                        <i class="icon-edit bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="#" class="tooltip-error" data-rel="tooltip"
                                                                   title="Delete">
                                                                    <span class="red">
                                                                        <i class="icon-trash bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>

                                                        </ul>

                                                    </div>

                                                </div>

                                            </td>

                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>

                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        @foreach($data['total_amount_to_be_paid'] as $total_amount_to_be_paid)
                                            @if($total_amount_to_be_paid->total_amount_to_be_paid)
                                                <th><p class="pull-right">Total Amount: Rs. {{ $total_amount_to_be_paid->total_amount_to_be_paid }} </p></th>
                                            @else
                                                <th><p class="pull-right">Total Amount: Rs. 0</p></th>
                                            @endif
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach($data['total_amount_paid'] as $total_amount_paid)
                                            @if($total_amount_paid->total_amount_paid)
                                                <th><p class="pull-right">Total Paid Amount: Rs. {{ $total_amount_paid->total_amount_paid }} </p></th>
                                            @else
                                                <th><p class="pull-right">Total Paid Amount: Rs. 0</p></th>
                                            @endif
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach($data['total_amount_to_be_paid'] as $total_amount_to_be_paid)
                                            @foreach($data['total_amount_paid'] as $total_amount_paid)
                                                <th><p class="pull-right">Total Due Amount: Rs. {{ $total_amount_to_be_paid->total_amount_to_be_paid - $total_amount_paid->total_amount_paid }} </p></th>
                                            @endforeach
                                        @endforeach
                                    </tr>
                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- page specific plugin scripts -->

    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.dataTables.bootstrap.js') }}"></script>

    <script type="text/javascript">
        jQuery(function ($) {

            var oTable1 = $('#sample-table-2').dataTable({
                "aoColumns": [
                    {"bSortable": false},
                    null, null, null, null,
                    {"bSortable": false}
                ]
            });

            $('table th input:checkbox').on('click', function () {
                var that = this;
                $(this).closest('table').find('tr > td:first-child input:checkbox')
                        .each(function () {
                            this.checked = that.checked;
                            $(this).closest('tr').toggleClass('selected');
                        });

            });


            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
                return 'left';
            }
        })
    </script>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/date-time/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/date-time/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/date-time/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/date-time/daterangepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('input[name=date-range]').daterangepicker().prev().on(ace.click_event, function(){
                $(this).next().focus();
            });
        });
    </script>

@endsection
