@extends('admin.layouts.master')

@section('title')
    View Payment
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>

                    <a href="{{ route($base_route.'.list') }}">Payment List</a>

                </li>

                <li class="active">View Payment</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Payment Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">
                                    <thead>

                                        <tr role="row">

                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 60px;">Column Name
                                            </th>

                                            <th role="columnheader" tabindex="0"
                                                aria-controls="sample-table-2" rowspan="1" colspan="1"
                                                style="width: 180px;">Column Value
                                            </th>

                                        </tr>

                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                        <tr class="odd">

                                            <td>Client Name</td>

                                            @foreach($data['client'] as $client)
                                                @if($client->id == $data['row']->client_id)
                                                    <td>{{ $client->name }} </td>
                                                @endif
                                            @endforeach

                                        </tr>

                                        <tr class="even">

                                            <td>Paid Amount</td>

                                            <td>Rs. {{ $data['row']->amount }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Created At</td>

                                            <td>{{ date('jS M, Y', strtotime($data['row']->created_at)) }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Updated At</td>

                                            <td>{{ date('jS M, Y', strtotime($data['row']->updated_at)) }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Created By</td>

                                            @foreach($data['user'] as $user)
                                                @if($user->id == $data['row']->user_id)
                                                    <td>{{ $user->fullname }}</td>
                                                @endif
                                            @endforeach

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
