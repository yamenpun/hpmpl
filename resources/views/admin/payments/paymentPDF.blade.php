<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Payment List</h2>

            <table>

                <tr>

                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 60px;">Created Date</th>

                    <th style="width: 80px;">Client Name</th>

                    <th style="width: 60px;">Amount</th>

                    <th style="width: 80px;">Created By</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->created_date }}</td>

                        <td>{{ $row->name }}</td>

                        <td>Rs. {{ $row->amount }}</td>

                        <td>{{ $row->fullname }}</td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
