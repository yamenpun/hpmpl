@extends('normal.layouts.master')

@section('title')
    Add Work Order
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ asset('assets/admin/css/chosen.css') }}" />

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Work Order List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Work Order Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                        'route'     => $base_route.'.store',
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'role'      => "form",
                        'enctype'   => "multipart/form-data"
                        ]) !!}

                    <input type="hidden" name="is_camp" value="0">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="entry_date">Entry Date <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div class="input-group">

                                {{ Form::date('entry_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                            </div>

                            {!! AppHelper::getValidationErrorMsg($errors, 'entry_date') !!}

                        </div>

                    </div>



                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="client_id">Client Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12" name="client_id"
                                    id="client_id" class="form-control">

                                <option>-- Select Client --</option>

                                @foreach($data['clients'] as $client)
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="patient_name">Patient Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('patient_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'patient_name',
                               "placeholder"                        => "Patient Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="age">Age <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::number('age', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'age',
                               "placeholder"                        => "Age",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="gender">Gender <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div class="control-group">

                                <div class="radio">
                                    <label>
                                        {!! Form::radio('gender', 'Male', true, ['class' => 'ace']) !!}
                                        <span class="lbl">Male</span>
                                    </label>

                                    <label>
                                        {!! Form::radio('gender', 'Female', false, ['class' => 'ace']) !!}
                                        <span class="lbl">Female</span>
                                    </label>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="referred_by">Ref By <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('referred_by', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'referred_by',
                               "placeholder"                        => "Ref By Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="test_names">Test Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select name="test_names[]" multiple="" class="width-80 chosen-select" data-placeholder="Choose Test...">

                                @foreach($data['tests'] as $test)

                                    <option value="{{ $test->name }}">{{ $test->name }}</option>

                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="amount">Amount <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('amount', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'amount',
                               "placeholder"                        => "Amount",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'amount') !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="serum_code">Serum Code <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('serum_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'serum_code',
                               "placeholder"                        => "Serum Code",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="blood_code">Blood Code</label>

                        <div class="col-sm-6">

                            {!! Form::text('blood_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'blood_code',
                               "placeholder"                        => "Blood Code",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="urine_code">Urine Code</label>

                        <div class="col-sm-6">

                            {!! Form::text('urine_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'urine_code',
                               "placeholder"                        => "Urine Code",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="fluoride_code">Fluoride Code</label>

                        <div class="col-sm-6">

                            {!! Form::text('fluoride_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'fluoride_code',
                               "placeholder"                        => "Fluoride Code",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="others_title">Others Title</label>

                        <div class="col-sm-6">

                            {!! Form::text('others_title', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'others_title',
                               "placeholder"                        => "Others Title",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="others_code">Others Code</label>

                        <div class="col-sm-6">

                            {!! Form::text('others_code', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'others_code',
                               "placeholder"                        => "Others Code",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="remarks">Remarks</label>

                        <div class="col-sm-6">

                            {!! Form::text('remarks', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'remarks',
                               "placeholder"                        => "Remarks",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'remarks') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/chosen.jquery.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $(".chosen-select").chosen();
        });
    </script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        });
    </script>

@endsection