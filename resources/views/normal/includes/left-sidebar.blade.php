<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('normal/dashboard')?'class="active"':'' !!}>
            <a href="{{ route('normal.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>

        <li {!! Request::is('normal/transactions/list*') || Request::is('normal/transactions/add*') || Request::is('normal/transactions/view*') || Request::is('normal/transactions/edit*') ?'class="active"':'' !!}>
            <a href="{{ route('normal.transactions.list') }}">
                <i class="icon-credit-card"></i>
                <span class="menu-text"> Work Order </span>
            </a>
        </li>

        <li {!! Request::is('normal/camp/list*') || Request::is('normal/camp/add*') || Request::is('normal/camp/view*') || Request::is('normal/camp/edit*') ?'class="active"':'' !!}>
            <a href="{{ route('normal.camp.list') }}">
                <i class="icon-plus-sign-alt"></i>
                <span class="menu-text"> Health Camp </span>
            </a>
        </li>

        <li {!! Request::is('normal/payments/list*') || Request::is('normal/payments/add*') || Request::is('normal/payments/view*') || Request::is('normal/payments/edit*') ?'class="active"':'' !!}>
            <a href="{{ route('normal.payments.list') }}">
                <i class="icon-shopping-cart"></i>
                <span class="menu-text"> Payments </span>
            </a>
        </li>

        <li {!! Request::is('normal/client/list*') || Request::is('normal/client/add*') || Request::is('normal/client/view*') || Request::is('normal/client/edit*') ?'class="active"':'' !!}>
            <a href="{{ route('normal.client.list') }}">
                <i class="icon-group"></i>
                <span class="menu-text"> Client List </span>
            </a>
        </li>

        <li {!! Request::is('normal/test/list*') || Request::is('normal/test/add*') || Request::is('normal/test/view*') || Request::is('normal/test/edit*') ?'class="active"':'' !!}>
            <a href="{{ route('normal.test.list') }}">
                <i class="icon-beaker"></i>
                <span class="menu-text"> Test List </span>
            </a>
        </li>

        <li>
            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>
        </li>
    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try {ace.settings.check('sidebar', 'collapsed')} catch (e) {}
    </script>
</div>
