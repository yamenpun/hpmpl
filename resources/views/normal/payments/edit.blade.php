@extends('normal.layouts.master')

@section('title')
    Update Payment
@endsection

@section('page_specific_style')
    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Payment List</a>
                </li>

                <li class="active">Update Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Update Payment Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-sm-12">

                    {!! Form::model($data['row'], [
                        'route'     => [$base_route.'.update', $data['row']->id],
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'role'      => "form",
                        'enctype'   => "multipart/form-data"
                        ]) !!}

                    <input type="hidden" name="id" value="{{ $data['row']->id }}">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="created_date">Payment Date <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <div class="input-group">

                                {{ Form::date('created_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                            </div>

                            {!! AppHelper::getValidationErrorMsg($errors, 'created-date') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="client_id"> Client Name <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12"
                                    name="client_id" id="client_id" class="form-control">

                                @foreach($data['client'] as $client)
                                    <?php
                                    $selected = false;

                                    if ($data['row']->client_id == $client->id) {
                                        $selected = true;
                                    }
                                    if (old('client_id')) {
                                        if (old('client_id') == $client->id) {
                                            $selected = true;
                                        } else {
                                            $selected = false;
                                        }
                                    }
                                    ?>
                                    <option value="{{ $client->id }}" {!! $selected?'selected=selected':'' !!}>{{ $client->name }}</option>
                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="due_amount"> Due Amount <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12"
                                    name="due_amount" id="due_amount" class="form-control">

                                <option value="" selected="selected">{{ $data['due_amount'][0]->due_amount }}</option>
                                <option value="">-- Please select client name first --</option>

                            </select>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label no-padding-right" for="amount">Amount <span style="color: #bf1d1d;">*</span></label>

                        <div class="col-sm-6">

                            {!! Form::text('amount', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'amount',
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'amount') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-3 col-md-9">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Update
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });
        });

        $('#client_id').on('change', function (e) {
            console.log(e);

            var client_id = e.target.value;

            // ajax
            $.get('/ajax-client-due?client_id=' + client_id, function (data) {
                // success data
                $('#due_amount').empty();
                $.each(data, function (index, dueAmountObj) {
                    $('#due_amount').append('<option value= "' + dueAmountObj.due_amount+'">'+dueAmountObj.due_amount+'</option>');
                })
            })
        })
    </script>

@endsection