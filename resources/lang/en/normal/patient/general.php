<?php
return [
    'column' => [
        'user'                  => 'Created By',
        'client-name'           => 'Client Name',
        'patient-name'          => 'Patient Name',
        'address'               => 'Address',
        'phone'                 => 'Phone',
        'email'                 => 'Email',
        'order'                 => 'Order',
        'status'                => 'Status',
        'is_deleted'            => 'Is Deleted',
        'created-at'            => 'Created At',
        'updated-at'            => 'Updated At',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                      => 'Patient',
        'page-manager'              => 'Patient Manager',
        'profile'                   => 'Profile',
        'list'                      => 'List Patient',
        'add-form'                  => 'Add Form',
        'add'                       => 'Add New Patient',
        'update-form'               => 'Update Form',
        'update'                    => 'Update Patient',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive',
        'yes'                       => 'Yes',
        'no'                        => 'No'
    ],
];