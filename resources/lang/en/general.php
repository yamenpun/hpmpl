<?php
return [

	'home' => 'HOME',

	'common' => [
		'add'                      => 'Add New',
		'add-submit'               => 'Save',
		'update-submit'            => 'Update',
		'delete-submit'			   => 'Delete',
		'reset'                    => 'Reset',
		'no-content-found' 	       => 'No Content Found.',
		'no-description-found' 	   => 'No Description Found.',
		'no-data-found' 	       => 'No Data Found.',
		'no-image-found' 	       => 'No Image Uploaded.',
		'no-caption-found' 	       => 'No Caption Found.',
		'no-document-found'        => 'No Document Uploaded.'

	],
];