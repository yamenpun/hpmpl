<?php
return [
    'content' => [
        'page'              => 'Dashboard',
        'page-manager'      => 'Dashboard Manager',
        'welcome'           => 'Welcome To Dashboard',
        'welcome-message'   => 'Welcome To HEALTH PARK MEDICO PVT. LTD.'
    ],
];