<?php
return [
    'column' => [
        'user'                  => 'Created By',
        'client-name'           => 'Client Name',
        'patient-name'          => 'Patient Name',
        'test-name'             => 'Test Name',
        'amount'                => 'Amount',
        'remarks'               => 'Remarks',
        'order'                 => 'Order',
        'status'                => 'Status',
        'is_deleted'            => 'Is Deleted',
        'created-at'            => 'Created At',
        'created-date'          => 'Date',
        'updated-at'            => 'Updated At',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                      => 'Transaction',
        'page-manager'              => 'Transaction Manager',
        'profile'                   => 'Profile',
        'list'                      => 'List of Transaction',
        'deleted-list'              => 'Deleted List of Transaction',
        'add-form'                  => 'Add Form',
        'add'                       => 'Add New Transaction',
        'update-form'               => 'Update Form',
        'update'                    => 'Update Transaction',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive',
    ],
];