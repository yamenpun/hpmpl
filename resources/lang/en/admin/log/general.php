<?php
return [
    'column' => [
        'user'                  => 'User',
        'action'                => 'Action',
        'description'           => 'Description',
        'is_deleted'            => 'Is Deleted',
        'created-at'            => 'Action Time',
        'updated-at'            => 'Updated At',
        'from-date'             => 'From',
        'to-date'               => 'To',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                      => 'User Log',
        'page-manager'              => 'User Log Manager',
        'list'                      => 'List of User Log',
        'delete-list'               => 'Delete User Log',
    ],
];