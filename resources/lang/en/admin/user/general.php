<?php
return [
    'column' => [
        'fullname'                  => 'Fullname',
        'username'                  => 'Username',
        'email'                     => 'Email',
        'password'                  => 'Password',
        'password-confirmation'     => 'Confirm Password',
        'new-password'              => 'New Password',
        'created-at'                => 'Created At',
        'updated-at'                => 'Updated At',
        'role'                      => 'User Type',
        'status'                    => 'Status',
        'action'                    => 'Action',
    ],

    'content' => [
        'page'                      => 'User',
        'page-manager'              => 'User Manager',
        'profile'                   => 'Profile',
        'user-profile'              => 'User Profile',
        'list'                      => 'List User',
        'add-form'                  => 'Add Form',
        'add'                       => 'Add New User',
        'update-form'               => 'Update Form',
        'update'                    => 'Update User',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive',
    ],
];