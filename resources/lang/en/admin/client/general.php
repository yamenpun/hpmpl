<?php
return [
    'column' => [
        'user'                  => 'Created By',
        'name'                  => 'Name',
        'address'               => 'Address',
        'phone'                 => 'Phone',
        'email'                 => 'Email',
        'order'                 => 'Order',
        'status'                => 'Status',
        'is_deleted'            => 'Is Deleted',
        'created-at'            => 'Created At',
        'updated-at'            => 'Updated At',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                      => 'Client',
        'page-manager'              => 'Client Manager',
        'profile'                   => 'Profile',
        'list'                      => 'List of Client',
        'deleted-list'              => 'Deleted List of Client',
        'add-form'                  => 'Add Form',
        'add'                       => 'Add New Client',
        'update-form'               => 'Update Form',
        'update'                    => 'Update Client',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive',
    ],
];