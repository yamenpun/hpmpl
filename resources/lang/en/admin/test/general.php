<?php
return [
    'column' => [
        'user'                  => 'Created By',
        'patient-name'          => 'Patient Name',
        'name'                  => 'Test Name',
        'description'           => 'Description',
        'rate'                  => 'Rate',
        'order'                 => 'Order',
        'status'                => 'Status',
        'is_deleted'            => 'Is Deleted',
        'created-at'            => 'Created At',
        'updated-at'            => 'Updated At',
        'action'                => 'Action',
    ],

    'content' => [
        'page'                      => 'Test',
        'page-manager'              => 'Test Manager',
        'profile'                   => 'Profile',
        'list'                      => 'List of Test',
        'deleted-list'              => 'Deleted List of Test',
        'add-form'                  => 'Add Form',
        'add'                       => 'Add New Test',
        'update-form'               => 'Update Form',
        'update'                    => 'Update Test',
        'password-confirmation'     => 'Retype Password',
        'active'                    => 'Active',
        'inactive'                  => 'Inactive'
    ],
];