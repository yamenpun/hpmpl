<?php
namespace App\HelperClass;

use Illuminate\Http\Request;

class AppHelper {

    /**
     * Generates html according to message_type and stores in
     * session flash storage
     *
     * @param $message_type bootstrap alert message type
     * @param $message html message
     */
    public function flash($message_type, $message)
    {
        $message_type = $this->checkBootstrapAlertClass($message_type);

        $message = "<div class=\"alert alert-" . $message_type . "\">
                        <button data-dismiss=\"alert\" class=\"close\" type=\"button\">
                            <i class=\"icon-remove\"></i>
                        </button>
                        " . $message . "
                        <br>
					</div>";

        request()->session()->flash('message', $message);
    }

    protected function checkBootstrapAlertClass($message_type)
    {
        $classes = ['info', 'success', 'warning', 'danger'];
        if (!in_array($message_type, $classes))
        {
            return 'info';
        }

        return $message_type;
    }

    public function getValidationErrorMsg($errors, $field_name)
    {
        if ($errors->has($field_name))
        {
            return '<strong class="help-block validation-error">' . $errors->first('caption_one') . "</strong>";
        }

        return '';
    }
    
    public function getClientFilter()
    {
        if (request()->has('client')) {
            return request()->get('client');
        } else {
            return 'all';
        }
    }

    public function getDateRangeFilter()
    {
        if(request()->has('date-range')){
            return request()->get('date-range');
        } else{
            return 'all';
        }
    }

    public function getPatientFilter()
    {
        if (request()->has('patient')) {
            return request()->get('patient');
        } else {
            return 'all';
        }
    }
    
}