<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'fullname', 'username', 'email', 'role', 'password', 'status', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client()
    {
        return $this->hasMany('App\Models\Client');
    }

    public function patient()
    {
        return $this->hasMany('App\Models\Patient');
    }

    public function test()
    {
        return $this->hasMany('App\Models\Test');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transactions');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payments');
    }


    public function activity_log()
    {
        return $this->hasMany('App\Models\ActivityLog');
    }

}
