<?php

namespace App\Models;

class Transactions extends BaseModel
{
    protected $table = 'transactions';

    protected $fillable = [
        'id','entry_date', 'user_id','client_id', 'patient_name','gender', 'age','referred_by', 'test_names','amount', 'serum_code','urine_code', 'blood_code', 'fluoride_code','others_title', 'others_code', 'remarks', 'is_deleted','is_camp', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
    
}
