<?php

namespace App\Models;

class Payments extends BaseModel
{
    protected $table = 'payments';

    protected $fillable = [
        'id', 'created_date', 'user_id', 'client_id', 'amount', 'due_amount', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
}
