<?php

namespace App\Models;

class ActivityLog extends BaseModel
{
    protected $table = 'activity_log';

    protected $fillable = [
        'id', 'user_id', 'action', 'description', 'is_deleted', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
