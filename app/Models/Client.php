<?php

namespace App\Models;


class Client extends BaseModel
{
    protected $table = 'client';

    protected $fillable = [
        'id', 'user_id', 'name', 'address', 'phone','email', 'is_deleted', 'due_amount','created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function transactions()
    {
        return $this->hasMany('App\Models\Transactions');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payments');
    }
}
