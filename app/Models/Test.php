<?php

namespace App\Models;

class Test extends BaseModel
{
    protected $table = 'test';

    protected $fillable = [
        'id', 'user_id', 'name','description', 'rate', 'is_deleted', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
