<?php

namespace App\Http\Controllers\Normal;

use Illuminate\Http\Request;
use App\Models\Test;
use App\Models\Transactions;
use Auth;
use Gate;
use DB;
use PDF;
use Excel;
use App\Models\Client;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Normal\Camp\AddFormValidation;
use App\Http\Requests\Normal\Camp\UpdateFormValidation;


class CampController extends NormalBaseController {

    protected $view_path = 'normal.camp';
    protected $base_route = 'normal.camp';
    protected $model;

    public function index(Request $requests)
    {
        $data = [];
        $data['clients']  = DB::select( DB::raw("
                  SELECT c.id, c.name FROM transactions AS t INNER JOIN 
                  client AS c ON t.client_id = c.id WHERE t.is_deleted = '0' AND t.is_camp = '1' GROUP BY c.id "));

        $client_name = $requests->input('client');
        $patient_name = $requests->input('patient');
        $date_range = $requests->input('date-range');

        if($client_name !== 'all' && $patient_name === null && $date_range == ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name === 'all' && $patient_name === null && $date_range == ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id 
                  WHERE t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name !== 'all' && $patient_name !== null && $date_range == ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.patient_name LIKE '%$patient_name%'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.patient_name LIKE '%$patient_name%'" ));

        }elseif($client_name === 'all' && $patient_name !== null && $date_range == ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name !== 'all' && $patient_name !== null && $date_range !== ""){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND c.name = '$client_name' AND t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND c.name = '$client_name' 
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  AND t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name === 'all' && $patient_name === null && $date_range !== ""){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.entry_date BETWEEN '$start_date' AND '$end_date'
                  AND t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name === 'all' && $patient_name !== null && $date_range !== ""){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  AND t.is_deleted = '0' AND t.is_camp = '1' " ));

        }elseif ($client_name !== 'all' && $patient_name === null && $date_range !== ""){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.id, c.name, t.patient_name, t.test_names, t.serum_code, t.blood_code, amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.is_deleted = '0' AND t.is_camp = '1'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY t.id DESC"));

            $data['total'] = DB::select( DB::raw("
                  SELECT  sum(amount) as total FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND  t.entry_date BETWEEN '$start_date' AND '$end_date'
                  AND t.is_deleted = '0' AND t.is_camp = '1' " ));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select( DB::raw("SELECT t.*, c.name, t.patient_name, t.test_names, u.fullname as user_name FROM transactions AS t 
                        INNER JOIN client AS c ON t.client_id = c.id 
                        INNER JOIN users as u on t.user_id = u.id WHERE t.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $data = [];

        $client_name = $requests->input('client');
        $patient_name = $requests->input('patient');
        $date_range = $requests->input('date-range');

        if($client_name === 'all' && $patient_name === 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.entry_date BETWEEN '$start_date' AND '$end_date' AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' 
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif($client_name !== 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.patient_name LIKE '%$patient_name%' 
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.patient_name LIKE '%$patient_name%' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name === 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name'
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }

        return view(parent::loadDefaultVars($this->view_path . '.campPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $data = [];

        $client_name = $requests->input('client');
        $patient_name = $requests->input('patient');
        $date_range = $requests->input('date-range');

        if($client_name === 'all' && $patient_name === 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.entry_date BETWEEN '$start_date' AND '$end_date' AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' 
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif($client_name !== 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.patient_name LIKE '%$patient_name%' 
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name' AND t.patient_name LIKE '%$patient_name%' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name === 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name'
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name !== 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE c.name = '$client_name'
                  AND t.entry_date BETWEEN '$start_date' AND '$end_date' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }elseif ($client_name === 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $data['rows'] = DB::select( DB::raw("
                  SELECT t.*, c.name, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.patient_name LIKE '%$patient_name%' 
                  AND t.is_deleted = '0' AND t.is_camp = '1'
                  ORDER BY t.id DESC"));

        }

        $pdf = PDF::loadView($this->view_path. '.campPDF', compact('data'));

        return $pdf->download('campPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $client_name = $requests->input('client');
        $patient_name = $requests->input('patient');
        $date_range = $requests->input('date-range');

        if($client_name === 'all' && $patient_name === 'all' && $date_range === 'all'){

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif ($client_name === 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->whereBetween('transactions.entry_date', [$start_date, $end_date])
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif ($client_name === 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('transactions.patient_name', 'LIKE', '%'.$patient_name.'%')
                ->whereBetween('transactions.entry_date', [$start_date, $end_date])
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $patient_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('transactions.patient_name', 'LIKE', '%'.$patient_name.'%')
                ->whereBetween('transactions.entry_date', [$start_date, $end_date])
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('transactions.patient_name', 'LIKE', '%'.$patient_name.'%')
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $patient_name === 'all' && $date_range === 'all'){

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $patient_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->whereBetween('transactions.entry_date', [$start_date, $end_date])
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }elseif($client_name === 'all' && $patient_name !== 'all' && $date_range === 'all'){

            $transaction = Transactions::join('client', 'client.id', '=', 'transactions.client_id')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->select('transactions.*', 'client.name', 'users.fullname')
                ->where('transactions.patient_name', 'LIKE', '%'.$patient_name.'%')
                ->where('transactions.is_deleted', '0')
                ->where('transactions.is_camp', '1')
                ->orderBy('transactions.id', 'ASC')
                ->get();

        }

        Excel::create('Camp Details', function($excel) use($transaction) {
            $excel->sheet('Sheet 1', function($sheet) use($transaction) {
                $sheet->fromArray($transaction);
            });
        })->export('xls');
    }

    public function create()
    {
        $data = [];

        $data['clients'] = Client::select('id', 'name')->where('is_deleted', '0')->get();

        $data['tests'] = Test::select('id', 'name')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();

        $tests = json_encode($request->get('test_names'));

        $tests1 = preg_replace("/[^a-zA-Z0-9.]+/", ",  ", $tests);

        $tests2 = ltrim($tests1, ',');

        $tests3 = str_replace('"', '', $tests2);

        $test_names = rtrim($tests3, ', ');

        Transactions::create([
            'entry_date'        =>      $request->get('entry_date'),
            'user_id'           =>      $user->id,
            'client_id'         =>      $request->get('client_id'),
            'patient_name'      =>      $request->get('patient_name'),
            'age'               =>      $request->get('age'),
            'gender'            =>      $request->get('gender'),
            'referred_by'       =>      $request->get('referred_by'),
            'test_names'        =>      $test_names,
            'amount'            =>      $request->get('amount'),
            'serum_code'        =>      $request->get('serum_code'),
            'urine_code'        =>      $request->get('urine_code'),
            'blood_code'        =>      $request->get('blood_code'),
            'fluoride_code'     =>      $request->get('fluoride_code'),
            'others_title'      =>      $request->get('others_title'),
            'others_code'       =>      $request->get('others_code'),
            'is_camp'           =>      $request->get('is_camp'),
            'remarks'           =>      $request->get('remarks'),
        ]);

        $due_amount = DB::table('client')
            ->where('id', $request->get('client_id'))
            ->select('due_amount')->get();

        $new_due_amount = $due_amount[0]->due_amount + $request->get('amount');

        DB::table('client')
            ->where('id', $request->get('client_id'))
            ->update(['due_amount' => $new_due_amount]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.add');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['client']  = Client::select('id', 'name')->where('is_deleted', '0')->get();

        $data['tests']    =  DB::table('test')->where('is_deleted', '0')->get();

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route . '.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        $due_amount = DB::table('client')
            ->where('id', $request->get('client_id'))
            ->select('due_amount')->get();

        $old_transaction_amount = DB::table('transactions')
            ->where('id', $id)
            ->select('amount')->get();

        $new_due_amount = $due_amount[0]->due_amount - $old_transaction_amount[0]->amount + $request->get('amount');

        $tests = json_encode($request->get('test_names'));

        $tests1 = preg_replace("/[^a-zA-Z0-9.]+/", ",  ", $tests);

        $tests2 = ltrim($tests1, ',');

        $tests3 = str_replace('"', '', $tests2);

        $test_names = rtrim($tests3, ', ');

        $data = $this->model;

        if($request->get('test_names') == ''){

            $data->update([
                'entry_date'        =>      $request->get('entry_date'),
                'user_id'           =>      $user->id,
                'client_id'         =>      $request->get('client_id'),
                'patient_name'      =>      $request->get('patient_name'),
                'age'               =>      $request->get('age'),
                'gender'            =>      $request->get('gender'),
                'referred_by'       =>      $request->get('referred_by'),
                'amount'            =>      $request->get('amount'),
                'serum_code'        =>      $request->get('serum_code'),
                'urine_code'        =>      $request->get('urine_code'),
                'blood_code'        =>      $request->get('blood_code'),
                'fluoride_code'     =>      $request->get('fluoride_code'),
                'others_title'      =>      $request->get('others_title'),
                'others_code'       =>      $request->get('others_code'),
                'remarks'           =>      $request->get('remarks'),
            ]);

        }else{

            $data->update([
                'entry_date'        =>      $request->get('entry_date'),
                'user_id'           =>      $user->id,
                'client_id'         =>      $request->get('client_id'),
                'patient_name'      =>      $request->get('patient_name'),
                'age'               =>      $request->get('age'),
                'gender'            =>      $request->get('gender'),
                'referred_by'       =>      $request->get('referred_by'),
                'test_names'        =>      $test_names,
                'amount'            =>      $request->get('amount'),
                'serum_code'        =>      $request->get('serum_code'),
                'urine_code'        =>      $request->get('urine_code'),
                'blood_code'        =>      $request->get('blood_code'),
                'fluoride_code'     =>      $request->get('fluoride_code'),
                'others_title'      =>      $request->get('others_title'),
                'others_code'       =>      $request->get('others_code'),
                'remarks'           =>      $request->get('remarks'),
            ]);

        }

        DB::table('client')
            ->where('id', $request->get('client_id'))
            ->update(['due_amount' => $new_due_amount]);

        $this->editLog($user->id, $request->get('patient_name'), 'Health Camp');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $health_camp = Transactions::where('id', '=', $id)->first();

        DB::table('transactions')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $client_id =  DB::table('transactions')->select('client_id', 'amount')
            ->where('id', $id)->get();

        $due_amount = DB::table('client')
            ->where('id', $client_id[0]->client_id)
            ->select('due_amount')->get();

        $new_due_amount = $due_amount[0]->due_amount - $client_id[0]->amount;

        DB::table('client')
            ->where('id', $client_id[0]->client_id)
            ->update(['due_amount' => $new_due_amount]);

        $this->deleteLog($user->id, $health_camp->patient_name, 'Health Camp');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Transactions::find($id);

        return $this->model;
    }
}
