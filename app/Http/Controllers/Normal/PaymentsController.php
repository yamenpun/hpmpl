<?php

namespace App\Http\Controllers\Normal;

use App\Models\ActivityLog;
use App\Models\Payments;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Gate;
use DB;
use App\Models\Client;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Payments\AddFormValidation;
use App\Http\Requests\Admin\Payments\UpdateFormValidation;
use Excel;
use PDF;

class PaymentsController extends NormalBaseController {

    protected $view_path = 'normal.payments';
    protected $base_route = 'normal.payments';
    protected $model;

    public function index(Request $requests)
    {
        $data = [];

        $data['clients'] = DB::select( DB::raw("
                  SELECT c.id,  c.name FROM 
                  payments AS p INNER JOIN client AS c ON p.client_id = c.id
                  WHERE p.is_deleted = '0'
                    GROUP BY c.id" ));

        $client_name = $requests->input('client');

        $date_range = $requests->input('date-range');

        if(($client_name === 'all' || $client_name === null) && $date_range == ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT p.id, p.amount, p.created_date, c.name, u.fullname FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0'
                  ORDER BY p.created_date DESC"));

            $data['total_amount_paid'] = DB::select( DB::raw("
                  SELECT sum(p.amount) as total_amount_paid FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' "));

            $data['total_amount_to_be_paid'] = DB::select( DB::raw("
                  SELECT sum(t.amount) as total_amount_to_be_paid FROM transactions AS t 
                  WHERE t.is_deleted = '0' "));

        }elseif ($client_name !== 'all' && $date_range === ""){

            $data['rows'] = DB::select( DB::raw("
                  SELECT p.id, p.amount, p.created_date, c.name, u.fullname FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND c.name = '$client_name'
                  ORDER BY p.created_date DESC"));

            $data['total_amount_paid'] = DB::select( DB::raw("
                  SELECT sum(p.amount) as total_amount_paid FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND c.name = '$client_name' "));

            $data['total_amount_to_be_paid'] = DB::select( DB::raw("
                  SELECT sum(t.amount) as total_amount_to_be_paid FROM transactions AS t 
                  INNER JOIN client as c on t.client_id = c.id
                  WHERE t.is_deleted = '0' AND c.name = '$client_name' "));

        } elseif ($client_name === 'all' && $date_range !== null){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT p.id, p.amount, p.created_date, c.name, u.fullname FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND p.created_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY p.created_date DESC"));

            $data['total_amount_paid'] = DB::select( DB::raw("
                  SELECT sum(p.amount) as total_amount_paid FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0'
                  AND p.created_date BETWEEN '$start_date' AND '$end_date' "));

            $data['total_amount_to_be_paid'] = DB::select( DB::raw("
                  SELECT sum(t.amount) as total_amount_to_be_paid FROM transactions AS t 
                  INNER JOIN client as c on t.client_id = c.id
                  WHERE t.is_deleted = '0' "));

        }elseif($client_name !== 'all' && $date_range !== ""){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] = DB::select( DB::raw("
                  SELECT p.id, p.amount, p.created_date, c.name, u.fullname FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND c.name = '$client_name' AND p.created_date BETWEEN '$start_date' AND '$end_date'
                  ORDER BY p.created_date DESC"));

            $data['total_amount_paid'] = DB::select( DB::raw("
                  SELECT sum(p.amount) as total_amount_paid FROM payments AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND c.name = '$client_name' 
                  AND p.created_date BETWEEN '$start_date' AND '$end_date' "));

            $data['total_amount_to_be_paid'] = DB::select( DB::raw("
                  SELECT sum(t.amount) as total_amount_to_be_paid FROM transactions AS t 
                  INNER JOIN client as c on t.client_id = c.id
                  WHERE t.is_deleted = '0' AND c.name = '$client_name' "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }
        $data = [];
        $data['user']    = User::select('id', 'fullname')->get();
        $data['client'] = Client::select('id', 'name')->get();
        $data['row']     = Payments::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $client_name = $requests->input('client');
        
        $date_range = $requests->input('date-range');

        if(($client_name === 'all' || $client_name === null) && $date_range == 'all'){

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name !== 'all' && $date_range == 'all'){

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();
        }

        return view(parent::loadDefaultVars($this->view_path . '.paymentPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $client_name = $requests->input('client');
        
        $date_range = $requests->input('date-range');

        if(($client_name === 'all' || $client_name === null) && $date_range == 'all'){

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name !== 'all' && $date_range == 'all'){

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $data['rows'] =  DB::table('payments')
                ->join('client', 'payments.client_id', '=', 'client.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->select('payments.*', 'client.name', 'users.fullname')
                ->where('client.name', $client_name)
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();
        }

        $pdf = PDF::loadView($this->view_path. '.paymentPDF', compact('data'));

        return $pdf->download('paymentPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $client_name = $requests->input('client');
        
        $date_range = $requests->input('date-range');

        if(($client_name === 'all' || $client_name === null) && $date_range == 'all'){

            $payments = Payments::join('client', 'client.id', '=', 'payments.client_id')
                ->join('users', 'users.id', '=', 'payments.user_id')
                ->select('payments.id', 'payments.created_date', 'client.name', 'payments.amount', 'users.fullname')
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name !== 'all' && $date_range == 'all'){

            $payments = Payments::join('client', 'client.id', '=', 'payments.client_id')
                ->join('users', 'users.id', '=', 'payments.user_id')
                ->select('payments.id', 'payments.created_date', 'client.name', 'payments.amount', 'users.fullname')
                ->where('client.name', $client_name)
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif ($client_name === 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $payments = Payments::join('client', 'client.id', '=', 'payments.client_id')
                ->join('users', 'users.id', '=', 'payments.user_id')
                ->select('payments.id', 'payments.created_date', 'client.name', 'payments.amount', 'users.fullname')
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }elseif($client_name !== 'all' && $date_range !== 'all'){

            $date = explode('-', $date_range);

            $start_date1 = rtrim($date[0]);

            $start_date2 = strtotime($start_date1);

            $start_date = date('Y-m-d', $start_date2);

            $end_date1 = ltrim($date[1]);

            $end_date2 = strtotime($end_date1);

            $end_date = date('Y-m-d', $end_date2);

            $payments = Payments::join('client', 'client.id', '=', 'payments.client_id')
                ->join('users', 'users.id', '=', 'payments.user_id')
                ->select('payments.id', 'payments.created_date', 'client.name', 'payments.amount', 'users.fullname')
                ->where('client.name', $client_name)
                ->whereBetween('payments.created_date', [$start_date, $end_date])
                ->where('payments.is_deleted', '0')
                ->orderBy('payments.id', 'ASC')
                ->get();

        }

        $paymentArray = [];

        // Define the Excel spreadsheet headers
        $paymentArray[] = ['S.N.', 'Payment Date', 'Client Name', 'Amount', 'Created By'];

        // Convert each member of the returned collection into an array,
        // and append it to the payment array.
        foreach ($payments as $payment) {
            $paymentArray[] = $payment->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Payments Detail Information', function($excel) use ($paymentArray) {

            // Build the spreadsheet, passing in the payment array
            $excel->sheet('sheet1', function($sheet) use ($paymentArray) {
                $sheet->fromArray($paymentArray, null, 'A1', false, false);
            });

        })->export('xls');

    }

    public function create()
    {
        $data = [];

        $data['clients'] = DB::select( DB::raw("
                  SELECT c.id,  c.name FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id
                   GROUP BY c.id" ));

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();

        Payments::create([
                'created_date'  => $request->get('created_date'),
                'client_id'     => $request->get('client_id'),
                'amount'        => $request->get('amount'),
                'user_id'       => $user->id,
            ]);

        $due_amount = DB::table('client')
            ->where('id', $request->get('client_id'))
            ->select('due_amount')->get();

        $new_due_amount = $due_amount[0]->due_amount - $request->get('amount');

        DB::table('client')
            ->where('id', $request->get('client_id'))
            ->update(['due_amount' => $new_due_amount]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.add');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['client'] = DB::select( DB::raw("
                  SELECT c.id,  c.name FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id
                   GROUP BY c.id" ));
        
        $data['due_amount'] = DB::select( DB::raw(" SELECT c.due_amount FROM payments as p INNER JOIN 
                              client AS c ON p.client_id = c.id WHERE p.id = '$id' "));

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        $due_amount = DB::table('client')
            ->where('id', $request->get('client_id'))
            ->select('due_amount')->get();

        $old_payment_amount = DB::table('payments')
            ->where('id', $id)
            ->select('amount')->get();

        $new_due_amount = $due_amount[0]->due_amount + $old_payment_amount[0]->amount - $request->get('amount');

        $data = $this->model;

        $data->update([
            'created_date'  => $request->get('created_date'),
            'client_id'     => $request->get('client_id'),
            'amount'        => $request->get('amount'),
            'user_id'       => $user->id,
        ]);

        DB::table('client')
            ->where('id', $request->get('client_id'))
            ->update(['due_amount' => $new_due_amount]);

        $this->editLogPayment($user->id, $request->get('amount'));

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $payment = Payments::where('id', '=', $id)->first();

        DB::table('payments')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $client_id =  DB::table('payments')->select('client_id', 'amount')
            ->where('id', $id)->get();

        $due_amount = DB::table('client')
                ->where('id', $client_id[0]->client_id)
                ->select('due_amount')->get();

        $new_due_amount = $due_amount[0]->due_amount + $client_id[0]->amount;

        DB::table('client')
            ->where('id', $client_id[0]->client_id)
            ->update(['due_amount' => $new_due_amount]);

        $this->deleteLogPayment($user->id, $payment->amount);

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Payments::find($id);

        return $this->model;
    }

    protected function editLogPayment($user, $record_name)
    {
        ActivityLog::create([
            'user_id'       => $user,
            'action'        => 'edit',
            'description'   => 'Record with amount Rs. '.$record_name.' was updated in Payment table.',
        ]);
    }

    protected function deleteLogPayment($user, $record_name)
    {
        ActivityLog::create([
            'user_id'       => $user,
            'action'        => 'delete',
            'description'   => 'Record with amount Rs. '.$record_name.' was deleted in Payment table.',
        ]);
    }

}
