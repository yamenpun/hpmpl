<?php

namespace App\Http\Controllers\Normal;

use Auth;
use Gate;
use DB;
use App\Models\Client;
use App\User;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Normal\Client\AddFormValidation;
use App\Http\Requests\Normal\Client\UpdateFormValidation;


class ClientController extends NormalBaseController {

    protected $view_path = 'normal.client';
    protected $base_route = 'normal.client';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw("SELECT c.*, u.fullname FROM client AS c 
                        INNER JOIN users AS u ON c.user_id = u.id
                        WHERE c.is_deleted = '0' ORDER BY c.id "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }
        $data = [];

        $data['user']    = User::select('id', 'fullname')->get();

        $data['row']     = Client::find($id);

        $data['transactions'] = DB::select( DB::raw("
                  SELECT t.patient_name, t.test_names, t.amount, t.entry_date, u.fullname FROM 
                  transactions AS t INNER JOIN client AS c ON t.client_id = c.id 
                  INNER JOIN users AS u ON t.user_id = u.id
                  WHERE t.is_deleted = '0' AND c.id = '$id'
                  ORDER BY t.id DESC"));

        $data['payments'] = DB::select( DB::raw("
                  SELECT p.amount, p.created_date, c.name, u.fullname FROM PAYMENTS AS p 
                  INNER JOIN client AS c ON p.client_id = c.id 
                  INNER JOIN users AS u ON p.user_id = u.id
                  WHERE p.is_deleted = '0' AND c.id = '$id'
                  ORDER BY p.created_date DESC"));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }



    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = Client::select('id', 'name', 'address', 'phone', 'email')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.clientPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = Client::select('id', 'name', 'address', 'phone', 'email')->where('is_deleted', '0')->get();

        $pdf = PDF::loadView($this->view_path. '.clientPDF', compact('data'));

        return $pdf->download('clientPDF.pdf');
    }

    public function exportAsExcel()
    {
        $client = Client::select('id', 'name', 'address', 'phone', 'email')->where('is_deleted', '0')->get();

        Excel::create('Client Details', function($excel) use($client) {
            $excel->sheet('Sheet 1', function($sheet) use($client) {
                $sheet->fromArray($client);
            });
        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();
        Client::create([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'phone'         => $request->get('phone'),
            'email'         => $request->get('email')
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.add');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'address'       => $request->get('address'),
            'phone'         => $request->get('phone'),
            'email'         => $request->get('email')
        ]);

        $this->editLog($user->id, $request->get('name'), 'Client');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $client = Client::where('id', '=', $id)->first();

        DB::table('client')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $client->name, 'Client');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Client::find($id);

        return $this->model;
    }
}
