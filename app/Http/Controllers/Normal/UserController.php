<?php

namespace App\Http\Controllers\Normal;

use Auth;
use Gate;
use AppHelper;
use App\Http\Requests;


class UserController extends NormalBaseController {

    protected $view_path = 'normal.user';
    protected $base_route = 'normal.user';

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }
}
