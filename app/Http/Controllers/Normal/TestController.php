<?php

namespace App\Http\Controllers\Normal;

use App\Models\Patient;
use App\Models\Test;
use Auth;
use Gate;
use DB;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Normal\Test\AddFormValidation;
use App\Http\Requests\Normal\Test\UpdateFormValidation;


class TestController extends NormalBaseController {

    protected $view_path = 'normal.test';
    protected $base_route = 'normal.test';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::table('test')
            ->select('test.*', 'users.fullname')
            ->join('users', 'test.user_id', '=', 'users.id')
            ->where('is_deleted', '0')
            ->orderBy('name', 'ASC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();
        Test::create([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'description'   => strip_tags($request->get('description')),
            'rate'          => $request->get('rate'),
            'status'        => $request->get('status'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'description'   => strip_tags($request->get('description')),
            'rate'          => $request->get('rate'),
            'status'        => $request->get('status'),
        ]);

        $this->editLog($user->id, $request->get('name'), 'Test');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $test = Test::where('id', '=', $id)->first();

        DB::table('test')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $test->name, 'Test');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Test::find($id);

        return $this->model;
    }
}
