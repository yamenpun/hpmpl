<?php

namespace App\Http\Controllers;

class HomeController extends AppBaseController
{

    public function index()
    {
        if(auth()->check()){

            $user = auth()->user();

            if ($user->role == 'normal') {
                return redirect()->route('normal.dashboard');
            } else {
                return redirect()->route('admin.dashboard');
            }
        }

        return view('auth.login');
    }
}
