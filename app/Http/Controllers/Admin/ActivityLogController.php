<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Log\AddFormValidation;
use App\Models\ActivityLog;
use Auth;
use Gate;
use DB;
use App\Models\UserLog;
use AppHelper;
use App\Http\Requests;


class ActivityLogController extends AdminBaseController {

    protected $view_path = 'admin.log';
    protected $base_route = 'admin.log';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::table('activity_log')
            ->select('activity_log.*', 'users.fullname')
            ->join('users', 'activity_log.user_id', '=', 'users.id')
            ->orderBy('activity_log.id', 'DESC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function deleteForm()
    {
        return view(parent::loadDefaultVars($this->view_path . '.deleteForm'));
    }

    public function delete(AddFormValidation $request)
    {
        $from_date = date('Y-m-d', strtotime($request->get('from-date')));
        $to_date   = date('Y-m-d', strtotime($request->get('to-date')));

        $affected = DB::delete("DELETE FROM activity_log WHERE created_at BETWEEN '$from_date' AND '$to_date'");

        if($affected > 0){
            AppHelper::flash('success', 'Record deleted Successfully.');
        }else{
            AppHelper::flash('danger', 'No Record Found To Delete.');
        }


        return redirect()->route($this->base_route.'.list');

    }
}
