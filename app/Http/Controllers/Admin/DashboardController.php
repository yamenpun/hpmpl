<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class DashboardController extends AdminBaseController
{
	protected $view_path = 'admin.dashboard';
    protected $base_route = 'admin.dashboard';

    public function index()
    {
        $data = [];

        $data['no_of_clients'] = DB::select( DB::raw("
              SELECT count(id) as no_of_clients FROM client"));

        $data['no_of_tests'] = DB::select( DB::raw("
              SELECT count(id) as no_of_tests FROM test "));

        $data['no_of_entry'] = DB::select( DB::raw(" SELECT count(id) as no_of_entry FROM transactions WHERE is_camp = '0' "));

        $data['total_amount_paid'] = DB::select( DB::raw("
              SELECT sum(p.amount) as total_amount_paid FROM payments AS p 
              INNER JOIN client AS c ON p.client_id = c.id 
              INNER JOIN users AS u ON p.user_id = u.id
              WHERE p.is_deleted = '0' "));

        $data['total_amount_to_be_paid'] = DB::select( DB::raw("
              SELECT sum(t.amount) as total_amount_to_be_paid FROM transactions AS t 
              WHERE t.is_deleted = '0' "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }
}
