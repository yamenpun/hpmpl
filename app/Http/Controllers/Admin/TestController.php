<?php

namespace App\Http\Controllers\Admin;

use App\Models\Test;
use App\User;
use Auth;
use Gate;
use DB;
use PDF;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Test\AddFormValidation;
use App\Http\Requests\Admin\Test\UpdateFormValidation;


class TestController extends AdminBaseController {

    protected $view_path = 'admin.test';
    protected $base_route = 'admin.test';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::table('test')
            ->select('test.*', 'users.fullname')
            ->join('users', 'test.user_id', '=', 'users.id')
            ->where('is_deleted', '0')
            ->orderBy('id', 'DESC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function isDeletedList()
    {
        $data = [];
        $data['rows'] = DB::table('test')
            ->select('test.*', 'users.fullname')
            ->join('users', 'test.user_id', '=', 'users.id')
            ->where('is_deleted', '1')
            ->orderBy('id', 'DESC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.deletedTestList'), compact('data'));
    }

    public function view($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }
        $data = [];
        $data['user']    = User::select('id', 'fullname')->get();
        $data['row']     = Test::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function deletedView($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }
        $data = [];
        $data['user']    = User::select('id', 'fullname')->get();
        $data['row']     = Test::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.viewDeleted'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.testPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '0')->get();

        $pdf = PDF::loadView($this->view_path. '.testPDF', compact('data'));

        return $pdf->download('testPDF.pdf');
    }

    public function exportAsExcel()
    {
        $test = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '0')->get();

        Excel::create('Test Details', function($excel) use($test) {
            $excel->sheet('Sheet 1', function($sheet) use($test) {
                $sheet->fromArray($test);
            });
        })->export('xls');
    }

    public function deletedExportAsPrint()
    {
        $data = [];
        $data['rows'] = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '1')->get();

        return view(parent::loadDefaultVars($this->view_path . '.deletedTestPrint'), compact('data'));
    }

    public function deletedExportAsPdf()
    {
        $data = [];
        $data['rows'] = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '1')->get();

        $pdf = PDF::loadView($this->view_path. '.deletedTestPDF', compact('data'));

        return $pdf->download('deletedTestPDF.pdf');
    }

    public function deletedExportAsExcel()
    {
        $test = Test::select('id', 'name', 'description', 'rate')->where('is_deleted', '1')->get();

        Excel::create('Deleted Test Details', function($excel) use($test) {
            $excel->sheet('Sheet 1', function($sheet) use($test) {
                $sheet->fromArray($test);
            });
        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();
        Test::create([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'description'   => strip_tags($request->get('description')),
            'rate'          => $request->get('rate')
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.add');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'user_id'       => $user->id,
            'name'          => $request->get('name'),
            'description'   => strip_tags($request->get('description')),
            'rate'          => $request->get('rate')
        ]);

        $this->editLog($user->id, $request->get('name'), 'Test');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $test = Test::where('id', '=', $id)->first();

        DB::table('test')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $test->name, 'Test');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('test')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $test = Test::where('id', '=', $id)->first();

        Test::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $test->name, 'Test');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Test::find($id);

        return $this->model;
    }
}
