<?php

use App\Models\Client;
use App\Models\Patient;
use Illuminate\Support\Facades\Input;

Route::get('/',                                     ['as' => 'home', 'uses' => 'HomeController@index']);

Route::auth();

Route::get('/ajax-client-due', function(){
   $client_id = Input::get('client_id');
    $clients = Client::where('id', '=', $client_id)->get();

    return Response::json($clients);
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('dashboard',                         ['as' => 'admin.dashboard',                     'uses' => 'Admin\DashboardController@index']);

    Route::get('user',                              ['as' => 'admin.user',                          'uses' => 'Admin\UserController@index']);
    Route::get('user/add',                          ['as' => 'admin.user.add',                      'uses' => 'Admin\UserController@create']);
    Route::post('user/store',                       ['as' => 'admin.user.store',                    'uses' => 'Admin\UserController@store']);
    Route::get('user/view/{id}',                    ['as' => 'admin.user.view',                     'uses' => 'Admin\UserController@view']);
    Route::get('user/edit/{id}',                    ['as' => 'admin.user.edit',                     'uses' => 'Admin\UserController@edit']);
    Route::post('user/update/{id}',                 ['as' => 'admin.user.update',                   'uses' => 'Admin\UserController@update']);
    Route::get('user/delete/{id}',                  ['as' => 'admin.user.delete',                   'uses' => 'Admin\UserController@destroy']);
    Route::get('user/profile',                      ['as' => 'admin.user.profile',                  'uses' => 'Admin\UserController@profile']);

    Route::get('client/list',                       ['as' => 'admin.client.list',                   'uses' => 'Admin\ClientController@index']);
    Route::get('client/add',                        ['as' => 'admin.client.add',                    'uses' => 'Admin\ClientController@create']);
    Route::post('client/store',                     ['as' => 'admin.client.store',                  'uses' => 'Admin\ClientController@store']);
    Route::get('client/view/{id}',                  ['as' => 'admin.client.view',                   'uses' => 'Admin\ClientController@view']);
    Route::get('client/deletedView/{id}',           ['as' => 'admin.client.deletedView',            'uses' => 'Admin\ClientController@deletedView']);
    Route::get('client/edit/{id}',                  ['as' => 'admin.client.edit',                   'uses' => 'Admin\ClientController@edit']);
    Route::post('client/update/{id}',               ['as' => 'admin.client.update',                 'uses' => 'Admin\ClientController@update']);
    Route::get('client/delete/{id}',                ['as' => 'admin.client.delete',                 'uses' => 'Admin\ClientController@destroy']);
    Route::get('client/deletedList',                ['as' => 'admin.client.deletedList',            'uses' => 'Admin\ClientController@isDeletedList']);
    Route::get('client/restore/{id}',               ['as' => 'admin.client.restore',                'uses' => 'Admin\ClientController@restore']);
    Route::get('client/delete-permanent/{id}',      ['as' => 'admin.client.delete-permanent',       'uses' => 'Admin\ClientController@deletePermanent']);

    Route::get('client/print',                      ['as' => 'admin.client.print',                  'uses' => 'Admin\ClientController@exportAsPrint']);
    Route::get('client/pdf',                        ['as' => 'admin.client.pdf',                    'uses' => 'Admin\ClientController@exportAsPdf']);
    Route::get('client/excel',                      ['as' => 'admin.client.excel',                  'uses' => 'Admin\ClientController@exportAsExcel']);

    Route::get('client/deletedPrint',               ['as' => 'admin.client.deletedPrint',           'uses' => 'Admin\ClientController@deletedExportAsPrint']);
    Route::get('client/deletedPdf',                 ['as' => 'admin.client.deletedPdf',             'uses' => 'Admin\ClientController@deletedExportAsPdf']);
    Route::get('client/deletedExcel',               ['as' => 'admin.client.deletedExcel',           'uses' => 'Admin\ClientController@deletedExportAsExcel']);

    Route::get('test/list',                         ['as' => 'admin.test.list',                     'uses' => 'Admin\TestController@index']);
    Route::get('test/add',                          ['as' => 'admin.test.add',                      'uses' => 'Admin\TestController@create']);
    Route::post('test/store',                       ['as' => 'admin.test.store',                    'uses' => 'Admin\TestController@store']);
    Route::get('test/view/{id}',                    ['as' => 'admin.test.view',                     'uses' => 'Admin\TestController@view']);
    Route::get('test/deletedView/{id}',             ['as' => 'admin.test.deletedView',              'uses' => 'Admin\TestController@deletedView']);
    Route::get('test/edit/{id}',                    ['as' => 'admin.test.edit',                     'uses' => 'Admin\TestController@edit']);
    Route::post('test/update/{id}',                 ['as' => 'admin.test.update',                   'uses' => 'Admin\TestController@update']);
    Route::get('test/delete/{id}',                  ['as' => 'admin.test.delete',                   'uses' => 'Admin\TestController@destroy']);
    Route::get('test/deletedList',                  ['as' => 'admin.test.deletedList',              'uses' => 'Admin\TestController@isDeletedList']);
    Route::get('test/restore/{id}',                 ['as' => 'admin.test.restore',                  'uses' => 'Admin\TestController@restore']);
    Route::get('test/delete-permanent/{id}',        ['as' => 'admin.test.delete-permanent',         'uses' => 'Admin\TestController@deletePermanent']);

    Route::get('test/print',                        ['as' => 'admin.test.print',                    'uses' => 'Admin\TestController@exportAsPrint']);
    Route::get('test/pdf',                          ['as' => 'admin.test.pdf',                      'uses' => 'Admin\TestController@exportAsPdf']);
    Route::get('test/excel',                        ['as' => 'admin.test.excel',                    'uses' => 'Admin\TestController@exportAsExcel']);

    Route::get('test/deletedPrint',                 ['as' => 'admin.test.deletedPrint',             'uses' => 'Admin\TestController@deletedExportAsPrint']);
    Route::get('test/deletedPdf',                   ['as' => 'admin.test.deletedPdf',               'uses' => 'Admin\TestController@deletedExportAsPdf']);
    Route::get('test/deletedExcel',                 ['as' => 'admin.test.deletedExcel',             'uses' => 'Admin\TestController@deletedExportAsExcel']);

    Route::get('log/list',                          ['as' => 'admin.log.list',                      'uses' => 'Admin\ActivityLogController@index']);
    Route::get('log/deleteForm',                    ['as' => 'admin.log.deleteForm',                'uses' => 'Admin\ActivityLogController@deleteForm']);
    Route::post('log/delete',                       ['as' => 'admin.log.delete',                    'uses' => 'Admin\ActivityLogController@delete']);

    Route::get('transactions/list',                  ['as' => 'admin.transactions.list',              'uses' => 'Admin\TransactionsController@index']);
    Route::get('transactions/add',                   ['as' => 'admin.transactions.add',               'uses' => 'Admin\TransactionsController@create']);
    Route::post('transactions/store',                ['as' => 'admin.transactions.store',             'uses' => 'Admin\TransactionsController@store']);
    Route::get('transactions/view/{id}',             ['as' => 'admin.transactions.view',              'uses' => 'Admin\TransactionsController@view']);
    Route::get('transactions/deletedView/{id}',      ['as' => 'admin.transactions.deletedView',       'uses' => 'Admin\TransactionsController@deletedView']);
    Route::get('transactions/edit/{id}',             ['as' => 'admin.transactions.edit',              'uses' => 'Admin\TransactionsController@edit']);
    Route::post('transactions/update/{id}',          ['as' => 'admin.transactions.update',            'uses' => 'Admin\TransactionsController@update']);
    Route::get('transactions/delete/{id}',           ['as' => 'admin.transactions.delete',            'uses' => 'Admin\TransactionsController@destroy']);
    Route::get('transactions/deletedList',           ['as' => 'admin.transactions.deletedList',       'uses' => 'Admin\TransactionsController@isDeletedList']);
    Route::get('transactions/restore/{id}',          ['as' => 'admin.transactions.restore',           'uses' => 'Admin\TransactionsController@restore']);
    Route::get('transactions/delete-permanent/{id}', ['as' => 'admin.transactions.delete-permanent',  'uses' => 'Admin\TransactionsController@deletePermanent']);

    Route::get('transactions/print',                 ['as' => 'admin.transactions.print',             'uses' => 'Admin\TransactionsController@exportAsPrint']);
    Route::get('transactions/pdf',                   ['as' => 'admin.transactions.pdf',               'uses' => 'Admin\TransactionsController@exportAsPdf']);
    Route::get('transactions/excel',                 ['as' => 'admin.transactions.excel',             'uses' => 'Admin\TransactionsController@exportAsExcel']);

    Route::get('transactions/deletedPrint',          ['as' => 'admin.transactions.deletedPrint',      'uses' => 'Admin\TransactionsController@deletedExportAsPrint']);
    Route::get('transactions/deletedPdf',            ['as' => 'admin.transactions.deletedPdf',        'uses' => 'Admin\TransactionsController@deletedExportAsPdf']);
    Route::get('transactions/deletedExcel',          ['as' => 'admin.transactions.deletedExcel',      'uses' => 'Admin\TransactionsController@deletedExportAsExcel']);

    Route::get('camp/list',                          ['as' => 'admin.camp.list',                      'uses' => 'Admin\CampController@index']);
    Route::get('camp/add',                           ['as' => 'admin.camp.add',                       'uses' => 'Admin\CampController@create']);
    Route::post('camp/store',                        ['as' => 'admin.camp.store',                     'uses' => 'Admin\CampController@store']);
    Route::get('camp/view/{id}',                     ['as' => 'admin.camp.view',                      'uses' => 'Admin\CampController@view']);
    Route::get('camp/deletedView/{id}',              ['as' => 'admin.camp.deletedView',               'uses' => 'Admin\CampController@deletedView']);
    Route::get('camp/edit/{id}',                     ['as' => 'admin.camp.edit',                      'uses' => 'Admin\CampController@edit']);
    Route::post('camp/update/{id}',                  ['as' => 'admin.camp.update',                    'uses' => 'Admin\CampController@update']);
    Route::get('camp/delete/{id}',                   ['as' => 'admin.camp.delete',                    'uses' => 'Admin\CampController@destroy']);
    Route::get('camp/deletedList',                   ['as' => 'admin.camp.deletedList',               'uses' => 'Admin\CampController@isDeletedList']);
    Route::get('camp/restore/{id}',                  ['as' => 'admin.camp.restore',                   'uses' => 'Admin\CampController@restore']);
    Route::get('camp/delete-permanent/{id}',         ['as' => 'admin.camp.delete-permanent',          'uses' => 'Admin\CampController@deletePermanent']);

    Route::get('camp/print',                         ['as' => 'admin.camp.print',                     'uses' => 'Admin\CampController@exportAsPrint']);
    Route::get('camp/pdf',                           ['as' => 'admin.camp.pdf',                       'uses' => 'Admin\CampController@exportAsPdf']);
    Route::get('camp/excel',                         ['as' => 'admin.camp.excel',                     'uses' => 'Admin\CampController@exportAsExcel']);

    Route::get('camp/deletedPrint',                  ['as' => 'admin.camp.deletedPrint',              'uses' => 'Admin\CampController@deletedExportAsPrint']);
    Route::get('camp/deletedPdf',                    ['as' => 'admin.camp.deletedPdf',                'uses' => 'Admin\CampController@deletedExportAsPdf']);
    Route::get('camp/deletedExcel',                  ['as' => 'admin.camp.deletedExcel',              'uses' => 'Admin\CampController@deletedExportAsExcel']);

    Route::get('payments/list',                      ['as' => 'admin.payments.list',                  'uses' => 'Admin\PaymentsController@index']);
    Route::get('payments/add',                       ['as' => 'admin.payments.add',                   'uses' => 'Admin\PaymentsController@create']);
    Route::post('payments/store',                    ['as' => 'admin.payments.store',                 'uses' => 'Admin\PaymentsController@store']);
    Route::get('payments/view/{id}',                 ['as' => 'admin.payments.view',                  'uses' => 'Admin\PaymentsController@view']);
    Route::get('payments/deletedView/{id}',          ['as' => 'admin.payments.deletedView',           'uses' => 'Admin\PaymentsController@deletedView']);
    Route::get('payments/edit/{id}',                 ['as' => 'admin.payments.edit',                  'uses' => 'Admin\PaymentsController@edit']);
    Route::post('payments/update/{id}',              ['as' => 'admin.payments.update',                'uses' => 'Admin\PaymentsController@update']);
    Route::get('payments/delete/{id}',               ['as' => 'admin.payments.delete',                'uses' => 'Admin\PaymentsController@destroy']);
    Route::get('payments/deletedList',               ['as' => 'admin.payments.deletedList',           'uses' => 'Admin\PaymentsController@isDeletedList']);
    Route::get('payments/restore/{id}',              ['as' => 'admin.payments.restore',               'uses' => 'Admin\PaymentsController@restore']);
    Route::get('payments/delete-permanent/{id}',     ['as' => 'admin.payments.delete-permanent',      'uses' => 'Admin\PaymentsController@deletePermanent']);

    Route::get('payments/print',                     ['as' => 'admin.payments.print',                 'uses' => 'Admin\PaymentsController@exportAsPrint']);
    Route::get('payments/pdf',                       ['as' => 'admin.payments.pdf',                   'uses' => 'Admin\PaymentsController@exportAsPdf']);
    Route::get('payments/excel',                     ['as' => 'admin.payments.excel',                 'uses' => 'Admin\PaymentsController@exportAsExcel']);

    Route::get('payments/deletedPrint',              ['as' => 'admin.payments.deletedPrint',          'uses' => 'Admin\PaymentsController@deletedExportAsPrint']);
    Route::get('payments/deletedPdf',                ['as' => 'admin.payments.deletedPdf',            'uses' => 'Admin\PaymentsController@deletedExportAsPdf']);
    Route::get('payments/deletedExcel',              ['as' => 'admin.payments.deletedExcel',          'uses' => 'Admin\PaymentsController@deletedExportAsExcel']);

});


Route::group(['prefix' => 'normal', 'middleware' => 'auth'], function(){

    Route::get('dashboard',                         ['as' => 'normal.dashboard',                     'uses' => 'Normal\DashboardController@index']);

    Route::get('user/profile',                      ['as' => 'normal.user.profile',                  'uses' => 'Normal\UserController@profile']);

    Route::get('client/list',                       ['as' => 'normal.client.list',                   'uses' => 'Normal\ClientController@index']);
    Route::get('client/add',                        ['as' => 'normal.client.add',                    'uses' => 'Normal\ClientController@create']);
    Route::post('client/store',                     ['as' => 'normal.client.store',                  'uses' => 'Normal\ClientController@store']);
    Route::get('client/view/{id}',                  ['as' => 'normal.client.view',                   'uses' => 'Normal\ClientController@view']);
    Route::get('client/edit/{id}',                  ['as' => 'normal.client.edit',                   'uses' => 'Normal\ClientController@edit']);
    Route::post('client/update/{id}',               ['as' => 'normal.client.update',                 'uses' => 'Normal\ClientController@update']);
    Route::get('client/delete/{id}',                ['as' => 'normal.client.delete',                 'uses' => 'Normal\ClientController@destroy']);

    Route::get('client/print',                      ['as' => 'normal.client.print',                  'uses' => 'Normal\ClientController@exportAsPrint']);
    Route::get('client/pdf',                        ['as' => 'normal.client.pdf',                    'uses' => 'Normal\ClientController@exportAsPdf']);
    Route::get('client/excel',                      ['as' => 'normal.client.excel',                  'uses' => 'Normal\ClientController@exportAsExcel']);

    Route::get('test/list',                         ['as' => 'normal.test.list',                     'uses' => 'Normal\TestController@index']);
    Route::get('test/add',                          ['as' => 'normal.test.add',                      'uses' => 'Normal\TestController@create']);
    Route::post('test/store',                       ['as' => 'normal.test.store',                    'uses' => 'Normal\TestController@store']);
    Route::get('test/view/{id}',                    ['as' => 'normal.test.view',                     'uses' => 'Normal\TestController@view']);
    Route::get('test/edit/{id}',                    ['as' => 'normal.test.edit',                     'uses' => 'Normal\TestController@edit']);
    Route::post('test/update/{id}',                 ['as' => 'normal.test.update',                   'uses' => 'Normal\TestController@update']);
    Route::get('test/delete/{id}',                  ['as' => 'normal.test.delete',                   'uses' => 'Normal\TestController@destroy']);

    Route::get('test/print',                        ['as' => 'normal.test.print',                    'uses' => 'Normal\TestController@exportAsPrint']);
    Route::get('test/pdf',                          ['as' => 'normal.test.pdf',                      'uses' => 'Normal\TestController@exportAsPdf']);
    Route::get('test/excel',                        ['as' => 'normal.test.excel',                    'uses' => 'Normal\TestController@exportAsExcel']);

    Route::get('transactions/list',                 ['as' => 'normal.transactions.list',             'uses' => 'Normal\TransactionsController@index']);
    Route::get('transactions/add',                  ['as' => 'normal.transactions.add',              'uses' => 'Normal\TransactionsController@create']);
    Route::post('transactions/store',               ['as' => 'normal.transactions.store',            'uses' => 'Normal\TransactionsController@store']);
    Route::get('transactions/view/{id}',            ['as' => 'normal.transactions.view',             'uses' => 'Normal\TransactionsController@view']);
    Route::get('transactions/edit/{id}',            ['as' => 'normal.transactions.edit',             'uses' => 'Normal\TransactionsController@edit']);
    Route::post('transactions/update/{id}',         ['as' => 'normal.transactions.update',           'uses' => 'Normal\TransactionsController@update']);
    Route::get('transactions/delete/{id}',          ['as' => 'normal.transactions.delete',           'uses' => 'Normal\TransactionsController@destroy']);

    Route::get('transactions/print',                ['as' => 'normal.transactions.print',            'uses' => 'Normal\TransactionsController@exportAsPrint']);
    Route::get('transactions/pdf',                  ['as' => 'normal.transactions.pdf',              'uses' => 'Normal\TransactionsController@exportAsPdf']);
    Route::get('transactions/excel',                ['as' => 'normal.transactions.excel',            'uses' => 'Normal\TransactionsController@exportAsExcel']);

    Route::get('camp/list',                         ['as' => 'normal.camp.list',                     'uses' => 'Normal\CampController@index']);
    Route::get('camp/add',                          ['as' => 'normal.camp.add',                      'uses' => 'Normal\CampController@create']);
    Route::post('camp/store',                       ['as' => 'normal.camp.store',                    'uses' => 'Normal\CampController@store']);
    Route::get('camp/view/{id}',                    ['as' => 'normal.camp.view',                     'uses' => 'Normal\CampController@view']);
    Route::get('camp/edit/{id}',                    ['as' => 'normal.camp.edit',                     'uses' => 'Normal\CampController@edit']);
    Route::post('camp/update/{id}',                 ['as' => 'normal.camp.update',                   'uses' => 'Normal\CampController@update']);
    Route::get('camp/delete/{id}',                  ['as' => 'normal.camp.delete',                   'uses' => 'Normal\CampController@destroy']);

    Route::get('camp/print',                        ['as' => 'normal.camp.print',                    'uses' => 'Normal\CampController@exportAsPrint']);
    Route::get('camp/pdf',                          ['as' => 'normal.camp.pdf',                      'uses' => 'Normal\CampController@exportAsPdf']);
    Route::get('camp/excel',                        ['as' => 'normal.camp.excel',                    'uses' => 'Normal\CampController@exportAsExcel']);

    Route::get('payments/list',                     ['as' => 'normal.payments.list',                 'uses' => 'Normal\PaymentsController@index']);
    Route::get('payments/add',                      ['as' => 'normal.payments.add',                  'uses' => 'Normal\PaymentsController@create']);
    Route::post('payments/store',                   ['as' => 'normal.payments.store',                'uses' => 'Normal\PaymentsController@store']);
    Route::get('payments/view/{id}',                ['as' => 'normal.payments.view',                 'uses' => 'Normal\PaymentsController@view']);
    Route::get('payments/edit/{id}',                ['as' => 'normal.payments.edit',                 'uses' => 'Normal\PaymentsController@edit']);
    Route::post('payments/update/{id}',             ['as' => 'normal.payments.update',               'uses' => 'Normal\PaymentsController@update']);
    Route::get('payments/delete/{id}',              ['as' => 'normal.payments.delete',               'uses' => 'Normal\PaymentsController@destroy']);

    Route::get('payments/print',                    ['as' => 'normal.payments.print',                'uses' => 'Normal\PaymentsController@exportAsPrint']);
    Route::get('payments/pdf',                      ['as' => 'normal.payments.pdf',                  'uses' => 'Normal\PaymentsController@exportAsPdf']);
    Route::get('payments/excel',                    ['as' => 'normal.payments.excel',                'uses' => 'Normal\PaymentsController@exportAsExcel']);

});






