<?php

namespace App\Http\Requests\Admin\Transactions;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'patient_name'      =>      'required',
            'age'               =>      'required|numeric',
            'referred_by'       =>      'required',
            'amount'            =>      'required|numeric',
            'serum_code'        =>      'required|unique:transactions,serum_code,'.$this->request->get('id').',id',
            'urine_code'        =>      'unique:transactions,urine_code,'.$this->request->get('id').',id',
            'blood_code'        =>      'unique:transactions,blood_code,'.$this->request->get('id').',id',
            'fluoride_code'     =>      'unique:transactions,fluoride_code,'.$this->request->get('id').',id',
            'others_title'      =>      'unique:transactions,others_title,'.$this->request->get('id').',id',
            'others_code'       =>      'unique:transactions,others_code,'.$this->request->get('id').',id',
        ];
    }
}
