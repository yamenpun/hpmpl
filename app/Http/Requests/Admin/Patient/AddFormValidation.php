<?php

namespace App\Http\Requests\Admin\Patient;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'  => 'required|unique:patient,fullname',
            'address'   => 'required',
            'email'     => 'email|unique:patient,email',
        ];
    }
}
