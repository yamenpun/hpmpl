<?php

namespace App\Http\Requests\Admin\Patient;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'fullname'  => 'required|unique:patient,fullname,'.$this->request->get('id').',id',
            'address'   => 'required',
            'email'     => 'email|unique:patient,email,'.$this->request->get('id').',id',
        ];
    }
}
