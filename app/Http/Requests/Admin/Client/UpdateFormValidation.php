<?php

namespace App\Http\Requests\Admin\Client;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name'      => 'required|unique:client,name,'.$this->request->get('id').',id',
            'address'   => 'required',
            'email'     => 'email|unique:client,email,'.$this->request->get('id').',id',
        ];
    }
}
