<?php

namespace App\Http\Requests\Admin\Log;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from-date'     => 'required|date',
            'to-date'       => 'required|date',
        ];
    }
    
}
