<?php

namespace App\Http\Requests\Normal\Payments;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'amount'     => 'required|numeric',
            'client_id'  => 'required',
            'due_amount' => 'required',
        ];
    }
}
