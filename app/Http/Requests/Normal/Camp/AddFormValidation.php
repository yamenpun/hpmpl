<?php

namespace App\Http\Requests\Normal\Camp;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient_name'      =>      'required',
            'age'               =>      'required|numeric',
            'referred_by'       =>      'required',
            'amount'            =>      'required|numeric',
            'test_names'        =>      'required',
            'serum_code'        =>      'required|unique:transactions,serum_code',
            'urine_code'        =>      'unique:transactions,urine_code',
            'blood_code'        =>      'unique:transactions,blood_code',
            'fluoride_code'     =>      'unique:transactions,fluoride_code',
            'others_title'      =>      'unique:transactions,others_title',
            'others_code'       =>      'unique:transactions,others_code',


        ];
    }
}
