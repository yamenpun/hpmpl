<?php

namespace App\Http\Requests\Normal\Test;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name'      => 'required|unique:test,name,'.$this->request->get('id').',id',
            'rate'      => 'required|numeric',
        ];
    }
}
