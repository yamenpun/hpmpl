<?php

namespace App\Http\Requests\Normal\Test;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:test,name',
            'rate'      => 'required|numeric',
        ];
    }
}
