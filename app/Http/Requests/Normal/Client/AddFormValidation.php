<?php

namespace App\Http\Requests\Normal\Client;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|unique:client,name',
            'address'   => 'required',
            'email'     => 'email|unique:client,email',
        ];
    }
}
